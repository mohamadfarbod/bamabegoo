<?php


Route::post('logout', 'Site\HomeController@postLogout');
Route::get('refereshcapcha', 'Site\HomeController@refereshCapcha');
Route::get('rahlog', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


//---------------------------------------- Admin ----------------------------------------------------------------

Route::group(array('prefix' => config('site.admin')), function () {

    Route::get('login', 'Auth\LoginController@getLogin');
    Route::post('login', 'Auth\LoginController@postLogin');

    Route::group(array('middleware' => 'AdminPermission'), function () {

        //---------------------User-----------------------------
        Route::get('user', 'Admin\UserController@getIndex');
        Route::get('user/add', 'Admin\UserController@getAdd');
        Route::post('user/add', 'Admin\UserController@postAdd');
        Route::get('user/edit/{id}', 'Admin\UserController@getEdit');
        Route::post('user/edit/{id}', 'Admin\UserController@postEdit');
        Route::post('user/delete', 'Admin\UserController@postDelete');
        //---------------------User Group-----------------------------
        Route::get('user/group', 'Admin\UserController@getGroup');
        Route::get('user/group-add', 'Admin\UserController@getGroupAdd');
        Route::post('user/group-add', 'Admin\UserController@postGroupAdd');
        Route::get('user/group-edit/{id}', 'Admin\UserController@getGroupEdit');
        Route::post('user/group-edit/{id}', 'Admin\UserController@postGroupEdit');
        Route::post('user/group-delete', 'Admin\UserController@postGroupDelete');
        //---------------------User Change Password-----------------------------
        Route::get('user/change-password', 'Admin\UserController@getChangePassword');
        Route::post('user/change-password', 'Admin\UserController@postChangePassword');
        //---------------------Setting-----------------------------
        Route::get('setting/edit', 'Admin\SettingController@getEdit');
        Route::post('setting/edit', 'Admin\SettingController@postEdit');
        //---------------------Page Category-----------------------------
        Route::get('page-category', 'Admin\PageCategoryController@getIndex');
        Route::get('page-category/add', 'Admin\PageCategoryController@getAdd');
        Route::post('page-category/add', 'Admin\PageCategoryController@postAdd');
        Route::get('page-category/edit/{id}', 'Admin\PageCategoryController@getEdit');
        Route::post('page-category/edit/{id}', 'Admin\PageCategoryController@postEdit');
        Route::post('page-category/delete', 'Admin\PageCategoryController@postDelete');
        Route::post('page-category/sort', 'Admin\PageCategoryController@postSort');
        //---------------------Page-----------------------------
        Route::get('pages', 'Admin\PagesController@getIndex');
        Route::get('pages/add', 'Admin\PagesController@getAdd');
        Route::post('pages/add', 'Admin\PagesController@postAdd');
        Route::get('pages/edit/{id}', 'Admin\PagesController@getEdit');
        Route::post('pages/edit/{id}', 'Admin\PagesController@postEdit');
        Route::post('pages/delete', 'Admin\PagesController@postDelete');
        Route::post('pages/sort', 'Admin\PagesController@postSort');
        //---------------------News-----------------------------
        Route::get('news', 'Admin\NewsController@getIndex');
        Route::get('news/add', 'Admin\NewsController@getAdd');
        Route::post('news/add', 'Admin\NewsController@postAdd');
        Route::get('news/edit/{id}', 'Admin\NewsController@getEdit');
        Route::post('news/edit/{id}', 'Admin\NewsController@postEdit');
        Route::post('news/delete', 'Admin\NewsController@postDelete');
        Route::post('news/sort', 'Admin\NewsController@postSort');
        //---------------------Brand-----------------------------
        Route::get('brand', 'Admin\BrandController@getIndex');
        Route::get('brand/add', 'Admin\BrandController@getAdd');
        Route::post('brand/add', 'Admin\BrandController@postAdd');
        Route::get('brand/edit/{id}', 'Admin\BrandController@getEdit');
        Route::post('brand/edit/{id}', 'Admin\BrandController@postEdit');
        Route::post('brand/delete', 'Admin\BrandController@postDelete');
        Route::post('brand/sort', 'Admin\BrandController@postSort');
        //---------------------Category-----------------------------
        Route::get('category', 'Admin\CategoryController@getIndex');
        Route::get('category/add', 'Admin\CategoryController@getAdd');
        Route::post('category/add', 'Admin\CategoryController@postAdd');
        Route::get('category/edit/{id}', 'Admin\CategoryController@getEdit');
        Route::post('category/edit/{id}', 'Admin\CategoryController@postEdit');
        Route::post('category/delete', 'Admin\CategoryController@postDelete');
        Route::post('category/sort', 'Admin\CategoryController@postSort');
        //---------------------Product-----------------------------
        Route::get('product', 'Admin\ProductController@getIndex');
        Route::get('product/add', 'Admin\ProductController@getAdd');
        Route::post('product/add', 'Admin\ProductController@postAdd');
        Route::get('product/edit/{id}', 'Admin\ProductController@getEdit');
        Route::post('product/edit/{id}', 'Admin\ProductController@postEdit');
        Route::post('product/delete', 'Admin\ProductController@postDelete');
        Route::post('product/sort', 'Admin\ProductController@postSort');
        //---------------------ProductTab-----------------------------
        Route::get('product-tab/list/{id}', 'Admin\ProductTabController@getList');
        Route::get('product-tab/add/{id}', 'Admin\ProductTabController@getAdd');
        Route::post('product-tab/add', 'Admin\ProductTabController@postAdd');
        Route::get('product-tab/edit/{id}', 'Admin\ProductTabController@getEdit');
        Route::post('product-tab/edit/{id}', 'Admin\ProductTabController@postEdit');
        Route::post('product-tab/delete', 'Admin\ProductTabController@postDelete');
        Route::post('product-tab/sort', 'Admin\ProductTabController@postSort');
        //---------------------ProductImage-----------------------------
        Route::get('product-image/list/{id}', 'Admin\ProductImageController@getList');
        Route::post('product-image/add', 'Admin\ProductImageController@postAdd');
        Route::post('product-image/delete', 'Admin\ProductImageController@postDelete');
        //---------------------Product Comment-----------------------------
        Route::get('product-comment', 'Admin\ProductCommentController@getIndex');
        Route::get('product-comment/edit/{id}', 'Admin\ProductCommentController@getEdit');
        Route::get('product-comment/active-reply/{id}', 'Admin\ProductCommentController@getActiveReply');
        Route::get('product-comment/reply/{id}', 'Admin\ProductCommentController@getReply');
        Route::post('product-comment/reply/{id}', 'Admin\ProductCommentController@postReply');
        Route::post('product-comment/ajax', 'Admin\ProductCommentController@postAjax');
        Route::post('product-comment/delete', 'Admin\ProductCommentController@postDelete');
        //---------------------Uploader-----------------------------
        Route::get('uploader', 'Admin\UploaderController@getIndex');
        Route::post('uploader/add', 'Admin\UploaderController@postAdd');
        Route::post('uploader/delete', 'Admin\UploaderController@postDelete');
        //---------------------Social-----------------------------
        Route::get('social', 'Admin\SocialController@getIndex');
        Route::get('social/add', 'Admin\SocialController@getAdd');
        Route::post('social/add', 'Admin\SocialController@postAdd');
        Route::get('social/edit/{id}', 'Admin\SocialController@getEdit');
        Route::post('social/edit/{id}', 'Admin\SocialController@postEdit');
        Route::post('social/delete', 'Admin\SocialController@postDelete');
        //---------------------Slider-----------------------------
        Route::get('slider', 'Admin\SliderController@getIndex');
        Route::get('slider/add', 'Admin\SliderController@getAdd');
        Route::post('slider/add', 'Admin\SliderController@postAdd');
        Route::get('slider/edit/{id}', 'Admin\SliderController@getEdit');
        Route::post('slider/edit/{id}', 'Admin\SliderController@postEdit');
        Route::post('slider/delete', 'Admin\SliderController@postDelete');
        Route::post('slider/sort', 'Admin\SliderController@postSort');
        //---------------------Contact US-----------------------------
        Route::get('contact-us', 'Admin\ContactUsController@getIndex');
        Route::get('contact-us/edit/{id}', 'Admin\ContactUsController@getEdit');
        Route::post('contact-us/delete', 'Admin\ContactUsController@postDelete');
        //---------------------SiteMap-----------------------------
        Route::get('site-map', 'Admin\SiteMapController@getIndex');

        Route::get('/', 'Admin\HomeController@getIndex');

    });
});

Route::get('product/list/{category_id}/{brand_id?}/{title1?}', 'Site\ProductController@getList');
Route::get('product/details/{id}', 'Site\ProductController@getDetails');
Route::get('product/search', 'Site\ProductController@getSearch');
Route::post('/comment', 'Site\HomeController@postComment');

Route::get('pages/list/{category_id}', 'Site\PagesController@getList');
Route::get('pages/details/{id}', 'Site\PagesController@getDetails');

Route::get('news/list', 'Site\NewsController@getList');
Route::get('news/details/{id}', 'Site\NewsController@getDetails');

Route::get('/about-us', 'Site\HomeController@getAboutUs');
Route::get('/contact-us', 'Site\HomeController@getContactUs');
Route::post('/contact-us', 'Site\HomeController@postContactUs');
Route::get('/', 'Site\HomeController@getIndex');