<?php

use Illuminate\Support\Facades\Artisan;

Route::group(array('prefix' => 'artisan', 'middleware' => 'ArtisanPermission'), function () {

    Route::get('/migrate', function () {
        Artisan::call('migrate');
        return 'Migrate Done!';
    });

    Route::get('/migrate:rollback', function () {
        Artisan::call('migrate:rollback');
        return 'migrate Rollback Done!';
    });

    Route::get('/view:clear', function () {
        Artisan::call('view:clear');
        return 'Ciew Clear Done!';
    });

    Route::get('/debugbar:clear', function () {
        Artisan::call('debugbar:clear');
        return 'Cebugbar Clear Done!';
    });

    Route::get('/cache:clear', function () {
        Artisan::call('cache:clear');
        return 'Cache Clear Done!';
    });

    Route::get('/config:clear', function () {
        Artisan::call('config:clear');
        return 'Config Clear Done!';
    });

    Route::get('/down', function () {
        Artisan::call('down');
        return 'Down Done!';
    });

    Route::get('/up', function () {
        Artisan::call('up');
        return 'Up Done!';
    });

});

