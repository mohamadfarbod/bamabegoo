<?php
return [
    'admin' => 'admin',
    'permisions' => [
        'user' => array(
            'title' => 'مدیران',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'group' => 'مشاهده دسترسی',
                'groupAdd' => 'اضافه دسترسی',
                'groupEdit' => 'ویرایش دسترسی',
                'groupDelete' => 'حذف دسترسی',
            )
        ),
        'category' => array(
            'title' => 'دسته بندی محصولات',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'sort' => 'مرتب سازی',
            )
        ),
        'brand' => array(
            'title' => 'دسته بندی دوم',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'sort' => 'مرتب سازی',
            )
        ),
        'product' => array(
            'title' => 'محصولات',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'sort' => 'مرتب سازی',
            )
        ),
        'product-image' => array(
            'title' => 'تصاویر بیشتر محصولات',
            'access' => array(
                'list' => 'مشاهده',
                'add' => 'اضافه',
                'delete' => 'حذف',
            )
        ),
        'product-tab' => array(
            'title' => 'تب های محصولات',
            'access' => array(
                'list' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'sort' => 'مرتب سازی',
            )
        ),
        'product-comment' => array(
            'title' => ' نظرات محصولات',
            'access' => array(
                'index' => 'مشاهده',
                'edit' => 'تایید',
                'reply' => 'پاسخ',
                'pageAjax' => 'جستجو دسته',
                'delete' => 'حذف',
            )
        ),
        'page-category' => array(
            'title' => 'دسته بندی صفحات ایستا',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'sort' => 'مرتب سازی',
            )
        ),
        'page' => array(
            'title' => 'صفحات ایستا',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'sort' => 'مرتب سازی',
            )
        ),
        'news' => array(
            'title' => 'اخبار',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'sort' => 'مرتب سازی',
            )
        ),
        'slider' => array(
            'title' => 'اسلایدر',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'sort' => 'مرتب سازی',
            )
        ),
        'social' => array(
            'title' => 'جوامع مجازی',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
            )
        ),
        'setting' => array(
            'title' => 'تنظیمات',
            'access' => array(
                'index' => 'مشاهده',
                'edit' => 'ویرایش',
            )

        ),
        'contact-us' => array(
            'title' => 'پیام های تماس با ما',
            'access' => array(
                'index' => 'مشاهده',
                'edit' => 'تایید',
                'delete' => 'حذف',
            )
        ),
        'uploader' => array(
            'title' => 'آپلودر',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'delete' => 'حذف',
            )
        ),
        'site-map' => array(
            'title' => 'سایت مپ',
            'access' => array(
                'index' => 'ساخت سایت مپ',
            )
        ),

    ]
];