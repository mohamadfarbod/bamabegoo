<?php

use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('email', 'info@rahweb.com')->exists()) {
            $input = [
                'name' => 'شرکت ره وب',
                'email' => 'info@rahweb.com',
                'admin' => 1,
                'status' => 1,
                'password' => bcrypt('new_pass'),
                'remember_token' => str_random(10),
            ];
            $data = User::create($input);

            $input = [
                'name' => 'مدیر کل',
                'permission' => 'a:2:{s:10:"fullAccess";i:1;s:4:"user";a:3:{s:3:"add";i:1;s:4:"edit";i:1;s:6:"delete";i:1;}}',
            ];
            $role = Role::create($input);

            $data->assignRole($role);
        }
    }
}
