<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSlider extends Migration
{
    public function up()
    {
        if (Schema::hasTable('slider')) {
            Schema::table('slider', function (Blueprint $table) {
                $table->string('link')->default(0);
                $table->boolean('is_banner')->default(0);
                $table->integer('place')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('slider')) {
            Schema::table('slider', function (Blueprint $table) {
                $table->dropColumn('link');
                $table->dropColumn('is_banner');
                $table->dropColumn('place');
            });
        }
    }
}
