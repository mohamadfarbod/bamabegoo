<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductTab extends Migration
{
    public function up()
    {
        Schema::create('product_tab', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('image')->nullable();
            $table->text('content')->nullable();
            $table->integer('product_id')->nullable();
            $table->boolean('status')->default(1);
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('product_tab');
    }
}
