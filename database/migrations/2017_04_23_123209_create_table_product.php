<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduct extends Migration
{
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('title_en')->nullable();
            $table->integer('category_id')->nullable();
            $table->text('lead')->nullable();
            $table->string('version')->nullable();
            $table->integer('update')->nullable();
            $table->string('android')->nullable();
            $table->string('builder')->nullable();
            $table->text('content')->nullable();
            $table->string('image')->nullable();
            $table->string('file')->nullable();
            $table->boolean('visit')->default(0);
            $table->boolean('status')->default(1);
            $table->boolean('show_side')->nullable();
            $table->boolean('show_first')->nullable();
            $table->boolean('show_menu')->nullable();
            $table->string('title_seo')->nullable();
            $table->string('keyword')->nullable();
            $table->text('description')->nullable();
            $table->boolean('listorder')->default(1);
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('product');
    }
}
