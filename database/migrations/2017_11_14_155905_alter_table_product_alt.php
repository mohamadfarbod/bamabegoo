<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductAlt extends Migration
{
    public function up()
    {
        if (Schema::hasTable('product')) {
            Schema::table('product', function (Blueprint $table) {
                $table->string('alt')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('product')) {
            Schema::table('product', function (Blueprint $table) {
                $table->string('alt');
            });
        }
    }
}
