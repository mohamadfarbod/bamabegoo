<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePageCategory extends Migration
{
    public function up()
    {
        Schema::create('page_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('parent_id')->nullable();
            $table->boolean('status')->default(1);
            $table->boolean('listorder')->default(1);
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('page_category');
    }
}
