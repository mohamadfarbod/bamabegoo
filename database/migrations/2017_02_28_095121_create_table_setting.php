<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSetting extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('setting')) {
            Schema::create('setting', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable();
                $table->text('about_us')->nullable();
                $table->text('contact_us')->nullable();
                $table->text('text1')->nullable();
                $table->text('text2')->nullable();
                $table->text('keyword')->nullable();
                $table->text('description')->nullable();
                $table->string('logo_water_mark')->nullable();
                $table->string('favicon')->nullable();
                $table->string('logo_header')->nullable();
                $table->string('logo_footer')->nullable();
                $table->text('map')->nullable();
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable('setting')) {
            Schema::drop('setting');
        }
    }
}
