<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductCommentsReply extends Migration
{
    public function up()
    {
        Schema::create('product_comments_reply', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('product_comment_id')->unsigned()->nullable()->index();
            $table->foreign('product_comment_id')->references('id')->on('product_comments');

            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->text('content')->nullable();
            $table->boolean('status')->default(0);
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('product_comments_reply');
    }
}
