<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductShow extends Migration
{
    public function up()
    {
        if (Schema::hasTable('product')) {
            Schema::table('product', function (Blueprint $table) {
                $table->boolean('under_slider')->default(0);
                $table->boolean('hot')->default(0);
                $table->boolean('vip')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('product')) {
            Schema::table('product', function (Blueprint $table) {
                $table->dropColumn('under_slider');
                $table->dropColumn('hot');
                $table->dropColumn('vip');
            });
        }
    }
}
