<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    protected $fillable = [
        'name', 'email', 'password', 'admin', 'status'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected function getDateFormat()
    {
        return 'U';
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }

    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }

    public function deleteRole($role)
    {
        return $this->roles()->detach($role);
    }

    public function hasPermission($access)
    {
        $accessCount = count(explode('.', $access));

        foreach ($this->roles as $userRole) {

            $allPermissions = unserialize($userRole->permission);
            if ($allPermissions['fullAccess'] == 1) {
                return true;
            }
            if ($accessCount == 1) {
                if (array_key_exists($access, $allPermissions)) {
                    return true;
                }
            }
            if (is_array($allPermissions)) {

                if (array_key_exists($access, array_dot($allPermissions))) {
                    return true;
                }

                return false;
            }

            return false;
        }

        return false;
    }
}
