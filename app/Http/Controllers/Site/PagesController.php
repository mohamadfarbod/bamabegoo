<?php

namespace App\Http\Controllers\Site;

use App\Models\Pages;
use App\Models\PageCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function getList($category_id)
    {
        $category = PageCategory::whereStatus(1)
            ->find($category_id);
        if (!$category) abort(404);

        if (count($category->childs)) {
            $categoryList = PageCategory::whereStatus(1)
                ->whereParentId($category_id)
                ->paginate(15);
            return view('site.pages.list-category')
                ->with('categoryList', $categoryList)
                ->with('category', $category);
        } else {
            $pages = Pages::whereCategoryId($category_id)
                ->whereStatus(1)
                ->latest()
                ->paginate(15);

            return view('site.pages.list')
                ->with('pages', $pages)
                ->with('category', $category);
        }
    }

    public function getDetails($id)
    {
        $pages = Pages::whereId($id)
            ->whereStatus(1)
            ->first();
        if (!$pages) abort(404);

        return view('site.pages.details')
            ->with('pages', $pages);
    }


}
