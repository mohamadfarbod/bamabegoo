<?php

namespace App\Http\Controllers\Site;

use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function getList()
    {
        $news = News::whereStatus(1)
            ->latest()
            ->paginate(15);

        return view('site.news.list')
            ->with('news', $news);
    }

    public function getDetails($id)
    {
        $news = News::whereId($id)
            ->whereStatus(1)
            ->first();
        if (!$news) abort(404);

        $newsLast = News::whereStatus(1)
            ->latest()
            ->take(5)
            ->get();

        return view('site.news.details')
            ->with('newsLast', $newsLast)
            ->with('news', $news);
    }


}
