<?php

namespace App\Http\Controllers\Site;

use App\Http\Requests\ContactUsRequest;
use App\Http\Requests\ProductCommentRequest;
use App\Models\Category;
use App\Models\ContactUs;
use App\Models\Product;
use App\Models\ProductComment;
use App\Models\ProductLike;
use App\Models\Slider;
use Egulias\EmailValidator\Warning\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Mews\Captcha\Facades\Captcha;

class HomeController extends Controller
{


    public function getIndex()
    {
        $slider = Slider::whereStatus(1)->slider()->orderby('listorder', 'ASC')->get();
        $banner = Slider::whereStatus(1)->banner1()->orderby('listorder', 'ASC')->get();

        $products = Product::whereStatus(1)->whereUnderSlider(1)->latest();
        $product1 = $products->take(1)->first();
        if ($product1) {
            $product2 = $products->where('id', '<>', $product1->id)->take(2)->get();
        } else {
            $product2 = [];
        }

        $productVip = Product::whereStatus(1)->whereVip(1)->latest()->take(3)->get();
        $productHot = Product::whereStatus(1)->whereHot(1)->latest()->take(10)->get();

        $category = Category::whereStatus(1)
            ->whereShowFirst(1)
            ->whereNull('parent_id')
            ->orderby('listorder', 'ASC')
            ->with('productFirst')
            ->get();

        return view('site.index')
            ->with('banner', $banner)
            ->with('productVip', $productVip)
            ->with('productHot', $productHot)
            ->with('category', $category)
            ->with('product1', $product1)
            ->with('product2', $product2)
            ->with('slider', $slider);
    }

    public function getContactUs()
    {
        return view('site.contact-us');
    }

    public function postContactUs(ContactUsRequest $request)
    {
        $input = $request->all();
        $input['status'] = 0;
        ContactUs::create($input);
        return Redirect::back()->with('success', 'پیام شما با موفقیت ثبت شد.');
    }

    public function getAboutUs()
    {
        return view('site.about-us');
    }

    public function refereshCapcha()
    {
        return Captcha::img();
    }

    public function postLogout()
    {
        Auth::logout();
        return Redirect::action('Site\HomeController@getIndex');
    }

    public function postComment(ProductCommentRequest $request)
    {
        $input = $request->all();
        $input['status'] = 0;
        ProductComment::create($input);
        return Redirect::back()->with('success', 'نظر شما با موفقیت ثبت شد.');
    }

}
