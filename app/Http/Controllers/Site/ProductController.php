<?php

namespace App\Http\Controllers\Site;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductLike;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    public function getList($category_id, $brand_id = null, $title1 = null)
    {
        $category = Category::whereStatus(1)
            ->find($category_id);
        if (!$category) abort(404);

        $brand = [];
        if ($brand_id != null) {
            $brand = Brand::whereStatus(1)
                ->find($brand_id);
            if (!$brand) abort(404);
        }

        $query = Product::query();
        $query->whereCategoryId($category_id);
        if ($brand_id != null)
            $query->whereBrandId($brand_id);
        $products = $query->whereStatus(1)
            ->latest()
            ->paginate(20);

        return view('site.product.list')
            ->with('brand', $brand)
            ->with('products', $products)
            ->with('category', $category);

    }

    public function getDetails($id)
    {
        $product = Product::whereId($id)
            ->whereStatus(1)
            ->with('productImage', 'productTab', 'comment', 'comment.replySite')
            ->first();
        if (!$product) abort(404);
        

        $banner = Slider::whereStatus(1)->banner2()->orderby('listorder', 'ASC')->get();

        $productRel = Product::whereCategoryId($product->category_id)
            ->whereBrandId($product->brand_id)
            ->where('id', '<>', $product->id)
            ->whereStatus(1)
            ->take(5)
            ->latest()
            ->get();

        return view('site.product.details')
            ->with('banner', $banner)
            ->with('productRel', $productRel)
            ->with('product', $product);
    }


    public function getSearch(Request $request)
    {
        $products = Product::where('title', 'LIKE', '%' . $request->get('search') . '%')
            ->whereStatus(1)
            ->paginate(15);
        return view('site.product.search')
            ->with('search', $request->get('search'))
            ->with('products', $products);
    }

}
