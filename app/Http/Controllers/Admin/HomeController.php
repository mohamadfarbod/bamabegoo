<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductComment;
use App\Models\ContactUs;
use App\Models\PageCategory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function getIndex()
    {
        $counts['user'] = User::whereAdmin(1)->whereStatus(1)->count();
        $counts['page-category'] = PageCategory::whereStatus(1)->count();
        $counts['category'] = Category::whereStatus(1)->count();
        $counts['product'] = Product::whereStatus(1)->count();

        $comments['product'] = ProductComment::with('product')->latest()->take(5)->get();
        $contact_us = ContactUs::latest()->take(5)->get();

        return view('admin.index')
            ->with('comments', $comments)
            ->with('contact_us', $contact_us)
            ->with('counts', $counts);
    }
}
