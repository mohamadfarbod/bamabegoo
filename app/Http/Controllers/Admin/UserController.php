<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function getIndex(Request $request)
    {
        $query = User::query();
        $query->whereAdmin(1);

        if ($request->has('search')) {

            if ($request->has('start') and $request->has('end')) {

                $start = explode('/', $request->get('start'));
                $end = explode('/', $request->get('end'));

                $s = jmktime(0, 0, 0, $start[1], $start[0], $start[2]);
                $e = jmktime(0, 0, 0, $end[1], $end[0], $end[2]);

                $query->whereBetween('created_at', array($s, $e));
            }

            if ($request->has('name')) {
                $query->where('name', 'LIKE', '%' . $request->get('name') . '%');
            }
            if ($request->has('email')) {
                $query->where('email', 'LIKE', '%' . $request->get('email') . '%');
            }
        }

        $data = $query->paginate(15);

        return View('admin.user.index')
            ->with('data', $data);
    }

    public function getAdd(Request $request)
    {

        $groups = Role::all();
        $groupsId = array();
        if ($request->has('group')) {
            foreach ($request->old('group') as $id) {
                $groupsId[] = $id;
            }
        }
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];

        return View('admin.user.add')
            ->with('status', $status)
            ->with('groups', $groups)
            ->with('groupsId', $groupsId);

    }


    public function postAdd(UserRequest $request)
    {
        $input = $request->all();
        $input['admin'] = 1;
        $input['password'] = bcrypt($request->get('password'));
        $user = User::create($input);
        if ($request->has('group')) {
            $user->assignRole($request['group']);
        }
        return Redirect::action('Admin\UserController@getIndex')->with('success', 'آیتم جدید اضافه شد.');
    }

    public function getEdit($id, Request $request)
    {
        $groups = Role::all();
        $data = User::find($id);
        $groupsId = array();

        foreach ($data->roles as $role) {
            $groupsId[] = $role->id;
        }

        if ($request->has('group')) {
            foreach ($request->old('group') as $id) {
                $groupsId[] = $id;
            }
        }
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];
        return View('admin.user.edit')
            ->with('status', $status)
            ->with('data', $data)
            ->with('groups', $groups)
            ->with('groupsId', $groupsId);

    }


    public function postEdit($id, UserRequest $request)
    {
        $rules = ['email' => 'required|email|unique:users,email,' . $id];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $input = Input::except(['password', '_token', 'repassword', 'group']);
            if ($request->has('password')) {
                if ($request->get('password') == $request->get('password')) {
                    $input['password'] = bcrypt($request->get('password'));
                }else{
                    return Redirect::back()->with('error','رمز عبور با تکرار آن یکی نمی باشد.');
                }
            }
            $user = User::find($id);
            $user->where('id', $id)->update($input);
            $user->roles()->detach();
            if ($request->has('group')) {
                $user->assignRole($request['group']);
            }
            return Redirect::action('Admin\UserController@getIndex')->with('success', 'آیتم مورد نظر با موفقیت ویرایش شد.');
        } else {
            return Redirect()->back()->withInput()->withErrors($validator);
        }
    }

    public function postDelete(UserRequest $request)
    {
        if (User::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\UserController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function getGroup()
    {
        $groups = Role::paginate(15);
        return View('admin.user.group.index')
            ->with('data', $groups);
    }

    public function getGroupAdd()
    {
        return View('admin.user.group.add');
    }

    public function postGroupAdd(UserRequest $request)
    {
        $role = new Role();
        $role->name = $request->get('name');
        $role->permission = serialize($request['access'] + ['fullAccess' => 0]);
        $role->save();
        if ($role->save()) {
            return Redirect::action('Admin\UserController@getGroup')->with('success', 'آیتم جدید اضافه شد.');
        }

    }

    public function getGroupEdit($id)
    {
        $data = Role::findorfail($id);
        return View('admin.user.group.edit')
            ->with('data', $data);
    }

    public function postGroupEdit($id, UserRequest $request)
    {
        $role = Role::find($id);
        $role->name = $request->get('name');
        $role->permission = serialize($request['access'] + ['fullAccess' => 0]);
        $role->save();

        if ($role->save()) {
            return Redirect::action('Admin\UserController@getGroup')->with('success', 'آیتم مورد نظر با موفقیت ویرایش شد.');
        }

    }


    public function postGroupDelete(UserRequest $request)
    {
        if (Role::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\UserController@getGroup')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function getChangePassword()
    {
        return View('admin.user.change');
    }

    public function postChangePassword(UserRequest $request)
    {
        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($request->get('password'));
        $user->save();

        if ($user->save()) {
            return Redirect::back()->with('success', 'آیتم مورد نظر با موفقیت ویرایش شد.');
        }

    }
}
