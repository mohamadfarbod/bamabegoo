<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Classes\MakeTree;
use Classes\UploadImg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
    public function getIndex(Request $request)
    {
        $query = Product::query();
        $query->with('category', 'brand');
        $setCat = false;
        $all = [];
        if ($request->has('search')) {
            if ($request->get('start') != null and $request->get('end') != null) {
                $start = explode('/', $request->get('start'));
                $end = explode('/', $request->get('end'));

                $s = jmktime(0, 0, 0, $start[1], $start[0], $start[2]);
                $e = jmktime(0, 0, 0, $end[1], $end[0], $end[2]);

                $query->whereBetween('created_at', array($s, $e));
            }
            if ($request->get('title') != null) {
                $query->where('title', 'LIKE', '%' . $request->get('title') . '%');
            }
            if ($request->get('category_id') != null) {
                $query->where('category_id', $request->get('category_id'));
                $setCat = true;
                $all = Product::whereCategoryId($request->get('category_id'))->orderBy('listorder', 'ASC')->get();
            }
            if ($request->get('id')) {
                $query->where('id', $request->get('id'));
            }
        }
        $data = $query->latest()->paginate(15);

        $category = Category::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $category = array(null => 'همه') + MakeTree::GenerateSelect();
        } else {
            $category = array(null => 'همه');
        }

        $brand = Brand::pluck('title', 'id')->all();
        $brand = ['' => 'همه'] + $brand;

        return View('admin.product.index')
            ->with('category', $category)
            ->with('setCat', $setCat)
            ->with('$brand', $brand)
            ->with('all', $all)
            ->with('data', $data);

    }

    public function getAdd()
    {
        $category = Category::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $category_id = array(null => 'بدون دسته') + MakeTree::GenerateSelect();
        } else {
            $category_id = array(null => 'بدون دسته');
        }

        $brand = Brand::pluck('title', 'id')->all();
        $brand = ['' => 'انتخاب کنید . . .'] + $brand;

        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];
        return View('admin.product.add')
            ->with('status', $status)
            ->with('brand', $brand)
            ->with('category_id', $category_id);

    }

    public function postAdd(ProductRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image')) {
            $path = "assets/uploads/product/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        $update = explode('/', $request->get('update'));
        if (count($update) == 3) {
            $update_date = jmktime(0, 0, 0, $update[1], $update[0], $update[2]);
            $input['update'] = $update_date;
        }else{
			 $input['update'] = null;
		}

        if ($request->hasFile('file')) {
            $pathMain = "assets/uploads/product/file/";
            $extension = $request->file('file')->getClientOriginalExtension();
            $ext = ['apk'];
            if (in_array($extension, $ext)) {
                $fileName = md5(microtime()) . ".$extension";
                $request->file('file')->move($pathMain, $fileName);
                $input['file'] = $fileName;
            } else {
                return Redirect::back()->with('error', 'فایل ارسالی صحیح نیست.');
            }
        }

        if ($request->has('category_id')) {
            $input['category_id'] = $request->get('category_id');
        } else {
            unset($input['category_id']);
        }
        $input['show_side'] = $request->has('show_side');
        $input['under_slider'] = $request->has('under_slider');
        $input['hot'] = $request->has('hot');
        $input['vip'] = $request->has('vip');
        $product = Product::create($input);

        return Redirect::action('Admin\ProductController@getIndex')
            ->with('success', 'آیتم جدید اضافه شد.');
    }

    public function getEdit($id)
    {

        $data = Product::findorfail($id);
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];

        $category = Category::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $category_id = array(null => 'بدون دسته') + MakeTree::GenerateSelect();
        } else {
            $category_id = array(null => 'بدون دسته');
        }

        $brand = Brand::pluck('title', 'id')->all();
        $brand = ['' => 'انتخاب کنید . . .'] + $brand;

        return View('admin.product.edit')
            ->with('category_id', $category_id)
            ->with('brand', $brand)
            ->with('data', $data)
            ->with('status', $status);

    }


    public function postEdit($id, ProductRequest $request)
    {
        $input = $request->all();
        $product = Product::find($id);

        if ($request->hasFile('image')) {
            $path = "assets/uploads/product/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            File::delete($path . 'big/' . $product->image);
            File::delete($path . 'medium/' . $product->image);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        $update = explode('/', $request->get('update'));
        if (count($update) == 3) {
            $update_date = jmktime(0, 0, 0, $update[1], $update[0], $update[2]);
            $input['update'] = $update_date;
        }else{
			 $input['update'] = null;
		}

        if ($request->hasFile('file')) {
            $pathMain = "assets/uploads/product/file/";
            File::delete($path . $product->file);
            $extension = $request->file('file')->getClientOriginalExtension();
            $ext = ['apk'];
            if (in_array($extension, $ext)) {
                $fileName = md5(microtime()) . ".$extension";
                $request->file('file')->move($pathMain, $fileName);
                $input['file'] = $fileName;
            } else {
                return Redirect::back()->with('error', 'فایل ارسالی صحیح نیست.');
            }
        }

        if ($request->has('category_id')) {
            $input['category_id'] = $request->get('category_id');
        } else {
            unset($input['category_id']);
        }

        $input['show_side'] = $request->has('show_side');
        $input['under_slider'] = $request->has('under_slider');
        $input['hot'] = $request->has('hot');
        $input['vip'] = $request->has('vip');
        $product->update($input);


        return Redirect::action('Admin\ProductController@getIndex')
            ->with('success', 'آیتم مورد نظر با موفقیت ویرابش شد.');
    }

    public function postDelete(ProductRequest $request)
    {
        $images = Product::whereIn('id', $request->get('deleteId'))->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/product/big/' . $item);
            File::delete('assets/uploads/product/medium/' . $item);
        }
        if (Product::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\ProductController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function postSort(ProductRequest $request)
    {
        if ($request->get('update') == "update") {
            $count = 1;
            if ($request->get('update') == 'update') {
                foreach ($request->get('arrayorder') as $idval) {
                    $category = Product::find($idval);
                    $category->listorder = $count;
                    $category->save();
                    $count++;
                }
                echo 'با موفقیت ذخیره شد.';
            }

        }

    }
}
