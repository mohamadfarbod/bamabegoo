<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PageCategoryRequest;
use App\Models\PageCategory;
use Classes\MakeTree;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class PageCategoryController extends Controller
{
    public function getIndex()
    {
        $data = PageCategory::all()->toArray();
        if (!empty($data)) {
            MakeTree::getData($data);
            $data = MakeTree::GenerateArray(array('paginate' => 15));
        }

        $categorySort = PageCategory::whereNull('parent_id')->orderby('listorder', 'ASC')->get();

        return View('admin.page-category.index')
            ->with('data', $data)
            ->with('categorySort', $categorySort);
    }

    public function getAdd()
    {
        $category = PageCategory::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $parent_id = array(null => 'بدون والد') + MakeTree::GenerateSelect();
        } else {
            $parent_id = array(null => 'بدون والد');
        }
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];
        return View('admin.page-category.add')
            ->with('status', $status)
            ->with('parent_id', $parent_id);
    }

    public function postAdd(PageCategoryRequest $request)
    {
        $input = $request->all();
        if ($request->has('parent_id')) {
            $input['parent_id'] = $request->get('parent_id');
        } else {
            unset($input['parent_id']);
        }
        PageCategory::create($input);
        return Redirect::action('Admin\PageCategoryController@getIndex')
            ->with('success', 'آیتم جدید اضافه شد.');
    }

    public function getEdit($id)
    {
        $data = PageCategory::findorfail($id);
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];

        $category = PageCategory::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $parent_id = array(null => 'بدون والد') + MakeTree::GenerateSelect();
        } else {
            $parent_id = array(null => 'بدون والد');
        }

        return View('admin.page-category.edit')
            ->with('parent_id', $parent_id)
            ->with('data', $data)
            ->with('status', $status);
    }


    public function postEdit($id, PageCategoryRequest $request)
    {
        $input = $request->all();
        $page_category = PageCategory::find($id);
        if ($request->has('parent_id')) {
            $input['parent_id'] = $request->get('parent_id');
        } else {
            $input['parent_id'] = null;
        }
        $page_category->update($input);
        return Redirect::action('Admin\PageCategoryController@getIndex')
            ->with('success', 'آیتم مورد نظر با موفقیت ویرابش شد.');
    }

    public function postDelete(PageCategoryRequest $request)
    {
        if (PageCategory::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\PageCategoryController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function postSort(PageCategoryRequest $request)
    {
        if ($request->get('update') == "update") {
            $count = 1;
            if ($request->get('update') == 'update') {
                foreach ($request->get('arrayorder') as $idval) {

                    $category = PageCategory::find($idval);
                    $category->listorder = $count;
                    $category->save();
                    $count++;
                }
                echo 'با موفقیت ذخیره شد.';
            }
        }
    }
}
