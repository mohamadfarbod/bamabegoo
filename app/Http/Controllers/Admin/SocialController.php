<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SocialRequest;
use App\Models\Social;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class SocialController extends Controller
{
    public function getIndex(Request $request)
    {
        $query = Social::query();
        if ($request->has('search')) {
            if ($request->has('start') and $request->has('end')) {
                $start = explode('/', $request->get('start'));
                $end = explode('/', $request->get('end'));

                $s = jmktime(0, 0, 0, $start[1], $start[0], $start[2]);
                $e = jmktime(0, 0, 0, $end[1], $end[0], $end[2]);

                $query->whereBetween('created_at', array($s, $e));
            }
            if ($request->has('title')) {
                $query->where('title', 'LIKE', '%' . $request->get('title') . '%');
            }
            if ($request->has('id')) {
                $query->where('id', $request->get('id'));
            }
        }
        $data = $query->latest()->paginate(15);

        return View('admin.social.index')
            ->with('data', $data);

    }

    public function getAdd()
    {
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];
        return View('admin.social.add')
            ->with('status', $status);
    }

    public function postAdd(SocialRequest $request)
    {
        $input = $request->all();
        Social::create($input);

        return Redirect::action('Admin\SocialController@getIndex')
            ->with('success', 'آیتم جدید اضافه شد.');
    }

    public function getEdit($id)
    {

        $data = Social::findorfail($id);
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];

        return View('admin.social.edit')
            ->with('data', $data)
            ->with('status', $status);
    }

    public function postEdit($id, SocialRequest $request)
    {
        $input = $request->all();
        $social = Social::find($id);
        $social->update($input);

        return Redirect::action('Admin\SocialController@getIndex')
            ->with('success', 'آیتم مورد نظر با موفقیت ویرابش شد.');
    }

    public function postDelete(SocialRequest $request)
    {
        if (Social::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\SocialController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }
}
