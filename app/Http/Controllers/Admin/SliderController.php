<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SliderRequest;
use App\Models\Slider;
use Classes\MakeTree;
use Classes\UploadImg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class SliderController extends Controller
{

    private $place = [
        '0' => 'اسلایدر',
        '1' => 'بنر 1',
        '2' => 'بنر 2',
        '3' => 'بنر 3',
        '4' => 'بنر 4',
        '5' => 'بنر 5',
    ];

    public function getIndex(Request $request)
    {
        $query = Slider::query();
        if ($request->has('search')) {
            if ($request->has('start') and $request->has('end')) {
                $start = explode('/', $request->get('start'));
                $end = explode('/', $request->get('end'));

                $s = jmktime(0, 0, 0, $start[1], $start[0], $start[2]);
                $e = jmktime(0, 0, 0, $end[1], $end[0], $end[2]);

                $query->whereBetween('created_at', array($s, $e));
            }
            if ($request->has('title')) {
                $query->where('title', 'LIKE', '%' . $request->get('title') . '%');
            }
            if ($request->has('id')) {
                $query->where('id', $request->get('id'));
            }
        }

        $data = $query->latest()->paginate(15);
        $sort = Slider::orderby('listorder', 'ASC')->get();

        return View('admin.slider.index')
            ->with('place', $this->place)
            ->with('sort', $sort)
            ->with('data', $data);

    }

    public function getAdd()
    {
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];

        return View('admin.slider.add')
            ->with('place', $this->place)
            ->with('status', $status);

    }

    public function postAdd(SliderRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image')) {
            $path = "assets/uploads/slider/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        if ($request->get('type') == 0) {
            $input['is_banner'] = 0;
        } else {
            $input['is_banner'] = 1;
        }

        Slider::create($input);
        return Redirect::action('Admin\SliderController@getIndex')
            ->with('success', 'آیتم جدید اضافه شد.');
    }

    public function getEdit($id)
    {

        $data = Slider::findorfail($id);
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];

        return View('admin.slider.edit')
            ->with('place', $this->place)
            ->with('data', $data)
            ->with('status', $status);
    }

    public function postEdit($id, SliderRequest $request)
    {
        $input = $request->all();
        $slider = Slider::find($id);

        if ($request->hasFile('image')) {
            $path = "assets/uploads/slider/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            File::delete($path . 'big/' . $slider->image);
            File::delete($path . 'medium/' . $slider->image);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }


        if ($request->get('type') == 0) {
            $input['is_banner'] = 0;
        } else {
            $input['is_banner'] = 1;
        }

        $slider->update($input);
        return Redirect::action('Admin\SliderController@getIndex')
            ->with('success', 'آیتم مورد نظر با موفقیت ویرابش شد.');
    }

    public function postDelete(SliderRequest $request)
    {
        $images = Slider::whereIn('id', $request->get('deleteId'))->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/slider/big/' . $item);
            File::delete('assets/uploads/slider/medium/' . $item);
        }
        if (Slider::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\SliderController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function postSort(SliderRequest $request)
    {
        if ($request->get('update') == "update") {
            $count = 1;
            if ($request->get('update') == 'update') {
                foreach ($request->get('arrayorder') as $idval) {
                    $category = Slider::find($idval);
                    $category->listorder = $count;
                    $category->save();
                    $count++;
                }
                echo 'با موفقیت ذخیره شد.';
            }
        }
    }
}
