<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UploaderRequest;
use App\Models\Uploader;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class UploaderController extends Controller
{
    public function getIndex(Request $request)
    {
        $query = Uploader::query();
        if ($request->has('search')) {
            if ($request->has('start') and $request->has('end')) {
                $start = explode('/', $request->get('start'));
                $end = explode('/', $request->get('end'));

                $s = jmktime(0, 0, 0, $start[1], $start[0], $start[2]);
                $e = jmktime(0, 0, 0, $end[1], $end[0], $end[2]);

                $query->whereBetween('created_at', array($s, $e));
            }
            if ($request->has('title')) {
                $query->where('title', 'LIKE', '%' . $request->get('title') . '%');
            }
            if ($request->has('id')) {
                $query->where('id', $request->get('id'));
            }
        }
        $data = $query->latest()->paginate(15);
        return View('admin.uploader.index')
            ->with('data', $data);
    }

    public function postAdd(UploaderRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('file')) {
            $pathMain = "assets/uploads/uploader/";
            $extension = $request->file('file')->getClientOriginalExtension();
            $ext = ['jpg', 'jpeg', 'png', 'xls', 'zip', 'rar', 'txt', 'doc', 'pdf', 'mov'];
            if (in_array($extension, $ext)) {
                $fileName = md5(microtime()) . ".$extension";
                $request->file('file')->move($pathMain, $fileName);
                $input['file'] = $fileName;
            } else {
                return Redirect::back()->with('error', 'فایل ارسالی صحیح نیست.');
            }

        }

        Uploader::create($input);
        return Redirect::action('Admin\UploaderController@getIndex')
            ->with('success', 'آیتم جدید اضافه شد.');
    }

    public function postDelete(UploaderRequest $request)
    {
        $images = Uploader::whereIn('id', $request->get('deleteId'))->pluck('file');
        foreach ($images as $item) {
            File::delete('assets/uploads/uploader/' . $item);
        }
        if (Uploader::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\UploaderController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }
}
