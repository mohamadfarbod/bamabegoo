<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\News;
use App\Models\Product;
use App\Models\category;
use App\Models\PageCategory;
use App\Models\Pages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class SiteMapController extends Controller
{
    public function getIndex()
    {
        $url = 'fa';
        $sitemap = App::make("sitemap");

        $sitemap->add(URL::action('Site\HomeController@getIndex'), date('Y-m-d H:i', time()), 1.0, 'daily');
        $sitemap->add(URL::action('Site\HomeController@getContactUs'), date('Y-m-d H:i', time()), 1.0, 'daily');
        $sitemap->add(URL::action('Site\HomeController@getAboutUs'), date('Y-m-d H:i', time()), 1.0, 'daily');


//_________________________________Product__________________________________________________

        $category = category::where('status', 1)->latest()->get();
        $brand = Brand::where('status', 1)->latest()->get();
        $products = Product::where('status', 1)->latest()->get();

        $sitemap->add(URL::action('Site\ProductController@getSearch'), date('Y-m-d H:i', time()), 1.0, 'daily');
        foreach ($category as $item) {
            $sitemap->add(URL::action('Site\ProductController@getList', $item->id), date('Y-m-d H:i', time()), 1.0, 'daily');
            foreach ($brand as $row) {
                $sitemap->add(URL::action('Site\ProductController@getList', [$item->id, $row->id]), date('Y-m-d H:i', time()), 1.0, 'daily');

            }
        }
        foreach ($products as $item) {
            $sitemap->add(URL::action('Site\ProductController@getDetails', $item->id), date('Y-m-d H:i', time()), 1.0, 'daily');

        }

//_________________________________Pages__________________________________________________

        $pageCategory = PageCategory::where('status', 1)->latest()->get();
        $pages = Pages::where('status', 1)->latest()->get();

        foreach ($pageCategory as $item) {
            $sitemap->add(URL::action('Site\PagesController@getList', $item->id), date('Y-m-d H:i', time()), 1.0, 'daily');

        }
        foreach ($pages as $item) {
            $sitemap->add(URL::action('Site\PagesController@getDetails', $item->id), date('Y-m-d H:i', time()), 1.0, 'daily');

        }
//_________________________________News__________________________________________________

        $news = News::where('status', 1)->latest()->get();

        $sitemap->add(URL::action('Site\NewsController@getList'), date('Y-m-d H:i', time()), 1.0, 'daily');
        foreach ($news as $item) {
            $sitemap->add(URL::action('Site\NewsController@getDetails', $item->id), date('Y-m-d H:i', time()), 1.0, 'daily');

        }

//____________________________________End SiteMap__________________________________________________

        if (!empty($sitemap->model->getItems())) {

            $sitemap->store('xml', '../public_html/sitemap');

//            $text = $sitemap->render('xml');
//            File::put('sitemap.xml', $text);

        }

        return Redirect::back()->with('success', 'با موفقیت انجام شد');
    }
}
