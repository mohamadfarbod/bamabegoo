<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewsRequest;
use App\Models\News;
use Classes\MakeTree;
use Classes\UploadImg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class NewsController extends Controller
{
    public function getIndex(Request $request)
    {
        $query = News::query();
        if ($request->has('search')) {
            if ($request->has('start') and $request->has('end')) {
                $start = explode('/', $request->get('start'));
                $end = explode('/', $request->get('end'));

                $s = jmktime(0, 0, 0, $start[1], $start[0], $start[2]);
                $e = jmktime(0, 0, 0, $end[1], $end[0], $end[2]);

                $query->whereBetween('created_at', array($s, $e));
            }
            if ($request->has('title')) {
                $query->where('title', 'LIKE', '%' . $request->get('title') . '%');
            }
            if ($request->has('id')) {
                $query->where('id', $request->get('id'));
            }
        }
        $data = $query->latest()->paginate(15);


        return View('admin.news.index')
            ->with('data', $data);

    }

    public function getAdd()
    {
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];
        return View('admin.news.add')
            ->with('status', $status);

    }

    public function postAdd(NewsRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image')) {
            $path = "assets/uploads/news/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }
        News::create($input);
        return Redirect::action('Admin\NewsController@getIndex')
            ->with('success', 'آیتم جدید اضافه شد.');
    }

    public function getEdit($id)
    {

        $data = News::findorfail($id);
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];

        return View('admin.news.edit')
            ->with('data', $data)
            ->with('status', $status);

    }


    public function postEdit($id, NewsRequest $request)
    {
        $input = $request->all();
        $news = News::find($id);

        if ($request->hasFile('image')) {
            $path = "assets/uploads/news/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            File::delete($path . 'big/' . $news->image);
            File::delete($path . 'medium/' . $news->image);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }
        $news->update($input);
        return Redirect::action('Admin\NewsController@getIndex')
            ->with('success', 'آیتم مورد نظر با موفقیت ویرابش شد.');
    }

    public function postDelete(NewsRequest $request)
    {
        $images = News::whereIn('id', $request->get('deleteId'))->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/news/big/' . $item);
            File::delete('assets/uploads/news/medium/' . $item);
        }
        if (News::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\NewsController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function postSort(NewsRequest $request)
    {
        if ($request->get('update') == "update") {
            $count = 1;
            if ($request->get('update') == 'update') {
                foreach ($request->get('arrayorder') as $idval) {
                    $category = News::find($idval);
                    $category->listorder = $count;
                    $category->save();
                    $count++;
                }
                echo 'با موفقیت ذخیره شد.';
            }

        }

    }
}
