<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use Classes\UploadImg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class BrandController extends Controller
{
    public function getIndex(Request $request)
    {
        $query = Brand::query();
        if ($request->has('search')) {
            if ($request->has('start') and $request->has('end')) {
                $start = explode('/', $request->get('start'));
                $end = explode('/', $request->get('end'));

                $s = jmktime(0, 0, 0, $start[1], $start[0], $start[2]);
                $e = jmktime(0, 0, 0, $end[1], $end[0], $end[2]);

                $query->whereBetween('created_at', array($s, $e));
            }
            if ($request->has('title')) {
                $query->where('title', 'LIKE', '%' . $request->get('title') . '%');
            }
            if ($request->has('id')) {
                $query->where('id', $request->get('id'));
            }
        }

        $data = $query->latest()->paginate(15);
        $sort = Brand::orderby('listorder', 'ASC')->get();

        return View('admin.brand.index')
            ->with('sort', $sort)
            ->with('data', $data);

    }

    public function getAdd()
    {
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];
        return View('admin.brand.add')
            ->with('status', $status);

    }

    public function postAdd(BrandRequest $request)
    {
        $input = $request->all();
        Brand::create($input);
        return Redirect::action('Admin\BrandController@getIndex')
            ->with('success', 'آیتم جدید اضافه شد.');
    }

    public function getEdit($id)
    {

        $data = Brand::findorfail($id);
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];
        return View('admin.brand.edit')
            ->with('data', $data)
            ->with('status', $status);
    }

    public function postEdit($id, BrandRequest $request)
    {
        $input = $request->all();
        $brand = Brand::find($id);

        $brand->update($input);
        return Redirect::action('Admin\BrandController@getIndex')
            ->with('success', 'آیتم مورد نظر با موفقیت ویرابش شد.');
    }

    public function postDelete(BrandRequest $request)
    {
        if (Brand::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\BrandController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function postSort(BrandRequest $request)
    {
        if ($request->get('update') == "update") {
            $count = 1;
            if ($request->get('update') == 'update') {
                foreach ($request->get('arrayorder') as $idval) {
                    $category = Brand::find($idval);
                    $category->listorder = $count;
                    $category->save();
                    $count++;
                }
                echo 'با موفقیت ذخیره شد.';
            }
        }
    }
}
