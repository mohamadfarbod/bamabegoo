<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductTabRequest;
use App\Models\ProductTab;
use Classes\UploadImg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ProductTabController extends Controller
{
    public function getList($product_id)
    {
        $data = ProductTab::whereProductId($product_id)->latest()->paginate(15);
        $all = ProductTab::all();
        return View('admin.product.tabs.index')
            ->with('product_id', $product_id)
            ->with('all', $all)
            ->with('data', $data);
    }


    public function getAdd($id)
    {
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];
        return View('admin.product.tabs.add')
            ->with('status', $status)
            ->with('product_id', $id);

    }

    public function postAdd(ProductTabRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image')) {
            $path = "assets/uploads/product/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        $product = ProductTab::create($input);

        return Redirect::action('Admin\ProductTabController@getList',$request->get('product_id'))
            ->with('success', 'آیتم جدید اضافه شد.');
    }

    public function getEdit($id)
    {

        $data = ProductTab::findorfail($id);
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];

        return View('admin.product.tabs.edit')
            ->with('data', $data)
            ->with('status', $status);

    }


    public function postEdit($id, ProductTabRequest $request)
    {
        $input = $request->all();
        $product = ProductTab::find($id);

        if ($request->hasFile('image')) {
            $path = "assets/uploads/product/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            File::delete($path . 'big/' . $product->image);
            File::delete($path . 'medium/' . $product->image);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        $product->update($input);


        return Redirect::action('Admin\ProductTabController@getList',$request->get('product_id'))
            ->with('success', 'آیتم مورد نظر با موفقیت ویرابش شد.');
    }


    public function postDelete(ProductTabRequest $request)
    {
        $images = ProductTab::whereIn('id', $request->get('deleteId'))->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/product/tabs/big/' . $item);
            File::delete('assets/uploads/product/tabs/medium/' . $item);
        }
        if (ProductTab::destroy($request->get('deleteId'))) {
            return Redirect::back()->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function postSort(ProductTabRequest $request)
    {
        if ($request->get('update') == "update") {
            $count = 1;
            if ($request->get('update') == 'update') {
                foreach ($request->get('arrayorder') as $idval) {
                    $category = ProductTab::find($idval);
                    $category->listorder = $count;
                    $category->save();
                    $count++;
                }
                echo 'با موفقیت ذخیره شد.';
            }

        }

    }
}
