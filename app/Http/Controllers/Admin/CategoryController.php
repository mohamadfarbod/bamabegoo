<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Classes\MakeTree;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    public function getIndex()
    {
        $data = Category::all()->toArray();
        if (!empty($data)) {
            MakeTree::getData($data);
            $data = MakeTree::GenerateArray(array('paginate' => 15));
        }

        $categorySort = Category::whereNull('parent_id')->orderby('listorder', 'ASC')->get();

        return View('admin.category.index')
            ->with('data', $data)
            ->with('categorySort', $categorySort);
    }

    public function getAdd()
    {
        $category = Category::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $parent_id = array(null => 'بدون والد') + MakeTree::GenerateSelect();
        } else {
            $parent_id = array(null => 'بدون والد');
        }
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];
        return View('admin.category.add')
            ->with('status', $status)
            ->with('parent_id', $parent_id);
    }

    public function postAdd(CategoryRequest $request)
    {
        $input = $request->all();
        if ($request->get('parent_id')) {
            $input['parent_id'] = $request->get('parent_id');
        } else {
            unset($input['parent_id']);
        }
        $input['show_first'] = $request->has('show_first');
        $input['show_first'] = $request->has('show_first');
        $input['show_menu'] = $request->has('show_menu');
        Category::create($input);
        return Redirect::action('Admin\CategoryController@getIndex')
            ->with('success', 'آیتم جدید اضافه شد.');
    }

    public function getEdit($id)
    {
        $data = Category::findorfail($id);
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];

        $category = Category::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $parent_id = array(null => 'بدون والد') + MakeTree::GenerateSelect();
        } else {
            $parent_id = array(null => 'بدون والد');
        }

        return View('admin.category.edit')
            ->with('parent_id', $parent_id)
            ->with('data', $data)
            ->with('status', $status);
    }


    public function postEdit($id, CategoryRequest $request)
    {
        $input = $request->all();
        $article_category = Category::find($id);
        if ($request->has('parent_id')) {
            $input['parent_id'] = $request->get('parent_id');
        } else {
            $input['parent_id'] = null;
        }
        $input['show_first'] = $request->has('show_first');
        $input['show_first'] = $request->has('show_first');
        $input['show_menu'] = $request->has('show_menu');
        $article_category->update($input);
        return Redirect::action('Admin\CategoryController@getIndex')
            ->with('success', 'آیتم مورد نظر با موفقیت ویرابش شد.');
    }

    public function postDelete(CategoryRequest $request)
    {
        if (Category::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\CategoryController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function postSort(CategoryRequest $request)
    {
        if ($request->get('update') == "update") {
            $count = 1;
            if ($request->get('update') == 'update') {
                foreach ($request->get('arrayorder') as $idval) {

                    $category = Category::find($idval);
                    $category->listorder = $count;
                    $category->save();
                    $count++;
                }
                echo 'با موفقیت ذخیره شد.';
            }
        }
    }
}
