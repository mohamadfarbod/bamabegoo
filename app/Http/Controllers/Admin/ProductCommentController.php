<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductCommentRequest;
use App\Models\ProductComment;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCommentReply;
use Classes\MakeTree;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ProductCommentController extends Controller
{
    public function getIndex(Request $request)
    {
        $query = ProductComment::query()->with('product');

        if ($request->has('search')) {
            if ($request->get('start') and $request->get('end')) {
                $start = explode('/', $request->get('start'));
                $end = explode('/', $request->get('end'));

                $s = jmktime(0, 0, 0, $start[1], $start[0], $start[2]);
                $e = jmktime(0, 0, 0, $end[1], $end[0], $end[2]);

                $query->whereBetween('created_at', array($s, $e));
            }
            if ($request->get('product_id')) {
                $query->where('product_id', $request->get('product_id'));
            }
        }
        $data = $query->latest()->paginate(15);

        $category = Category::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $category = ['' => 'همه'] + MakeTree::GenerateSelect();
        } else {
            $category = ['' => 'همه'];
        }

        return View('admin.product-comment.index')
            ->with('category', $category)
            ->with('data', $data);

    }

    public function getEdit($id)
    {
        $product_comment = ProductComment::find($id);
        $product_comment->status = !$product_comment->status;
        $product_comment->save();
        return Redirect::back();
    }

    public function getReply($id)
    {
        $data = ProductComment::find($id);
        return View('admin.product-comment.reply')
            ->with('data', $data);
    }

    public function postReply($id, ProductCommentRequest $request)
    {
        $product_comment = ProductComment::with('reply', 'product')->find($id);
        $input = [
            'content' => $request->get('content'),
            'product_comment_id' => $id,
            'name' => Auth::user()->name . ' ' . Auth::user()->family,
            'user_id' => Auth::id(),
            'status' => 1,
        ];
        ProductCommentReply::create($input);
        return Redirect::action('Admin\ProductCommentController@getIndex')
            ->with('success', 'پاسخ شما ثبت شد.');
    }

    public function getActiveReply($id)
    {
        $c_reply = ProductCommentReply::find($id);
        $c_reply->status = !$c_reply->status;
        $c_reply->save();
        return Redirect::back();
    }

    public function postAjax(Request $request)
    {
        $input = $request->all();
        $product = Product::whereCategoryId($input['key'])
            ->select('id', 'title')
            ->get();

        $returnValue['value'] = "";
        foreach ($product as $row) {
            $returnValue['value'] .= "<option value='$row->id'>" . $row->title . "</option>";
        }

        $returnValue['status'] = true;
        return json_encode($returnValue);
    }

    public function postDelete(ProductCommentRequest $request)
    {
        if (ProductComment::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\ProductCommentController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

}
