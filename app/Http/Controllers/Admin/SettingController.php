<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SettingRequest;
use App\Models\Setting;

use Classes\UploadImg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class SettingController extends Controller
{
    public function getEdit()
    {
        $data = Setting::first();
        return View('admin.setting.edit')
            ->with('data', $data);

    }

    public function postEdit(SettingRequest $request)
    {
        $input = $request->all();

        $setting = Setting::first();

        if ($request->hasFile('logo_water_mark')) {
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('logo_water_mark'), 'assets/uploads/setting/', false);
            $pathMain = 'assets/uploads/setting/main/';
            File::delete($pathMain . $setting->logo_water_mark);
            if ($fileName) {
                $input['logo_water_mark'] = $fileName;
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }


        if ($request->hasFile('logo_footer')) {
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('logo_footer'), 'assets/uploads/setting/', false);
            $pathMain = 'assets/uploads/setting/main/';
            File::delete($pathMain . $setting->logo_footer);
            if ($fileName) {
                $input['logo_footer'] = $fileName;
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        if ($request->hasFile('logo_header')) {
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('logo_header'), 'assets/uploads/setting/', false);
            $pathMain = 'assets/uploads/setting/main/';
            File::delete($pathMain . $setting->logo_header);
            if ($fileName) {
                $input['logo_header'] = $fileName;
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        if ($request->hasFile('logo_header_gallery')) {
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('logo_header_gallery'), 'assets/uploads/setting/', false);
            $pathMain = 'assets/uploads/setting/main/';
            File::delete($pathMain . $setting->logo_header_gallery);
            if ($fileName) {
                $input['logo_header_gallery'] = $fileName;
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        if ($request->hasFile('favicon')) {
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('favicon'), 'assets/uploads/setting/', false);
            $pathMain = 'assets/uploads/setting/main/';
            File::delete($pathMain . $setting->favicon);
            if ($fileName) {
                $input['favicon'] = $fileName;
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        $setting->update($input);

        return Redirect::action('Admin\SettingController@getEdit')->with('success', 'آیتم مورد نظر با موفقیت ویرایش شد.');

    }
}
