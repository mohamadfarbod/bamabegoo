<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PagesRequest;
use App\Models\PageCategory;
use App\Models\Pages;
use Classes\MakeTree;
use Classes\UploadImg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class PagesController extends Controller
{
    public function getIndex(Request $request)
    {
        $query = Pages::query();
        $setCat = false;
        $all = [];
        if ($request->has('search')) {
            if ($request->has('start') and $request->has('end')) {
                $start = explode('/', $request->get('start'));
                $end = explode('/', $request->get('end'));

                $s = jmktime(0, 0, 0, $start[1], $start[0], $start[2]);
                $e = jmktime(0, 0, 0, $end[1], $end[0], $end[2]);

                $query->whereBetween('created_at', array($s, $e));
            }
            if ($request->has('title')) {
                $query->where('title', 'LIKE', '%' . $request->get('title') . '%');
            }
            if ($request->has('category_id')) {
                $query->where('category_id', $request->get('category_id'));
                $setCat = true;
                $all = Pages::whereCategoryId($request->get('category_id'))->orderBy('listorder', 'ASC')->get();
            }
            if ($request->has('id')) {
                $query->where('id', $request->get('id'));
            }
        }
        $data = $query->latest()->paginate(15);

        $category = PageCategory::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $category = array(null => 'بدون دسته') + MakeTree::GenerateSelect();
        } else {
            $category = array(null => 'بدون دسته');
        }

        return View('admin.pages.index')
            ->with('category', $category)
            ->with('setCat', $setCat)
            ->with('all', $all)
            ->with('data', $data);

    }

    public function getAdd()
    {
        $category = PageCategory::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $category_id = array(null => 'بدون دسته') + MakeTree::GenerateSelect();
        } else {
            $category_id = array(null => 'بدون دسته');
        }
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];
        return View('admin.pages.add')
            ->with('status', $status)
            ->with('category_id', $category_id);

    }

    public function postAdd(PagesRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image')) {
            $path = "assets/uploads/pages/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        if ($request->has('category_id')) {
            $input['category_id'] = $request->get('category_id');
        } else {
            unset($input['category_id']);
        }
        $input['show_side'] = $request->has('show_side');
        $input['show_first'] = $request->has('show_first');
        $input['show_menu'] = $request->has('show_menu');
        Pages::create($input);
        return Redirect::action('Admin\PagesController@getIndex')
            ->with('success', 'آیتم جدید اضافه شد.');
    }

    public function getEdit($id)
    {

        $data = Pages::findorfail($id);
        $status = [
            '1' => 'فعال',
            '0' => 'غیر فعال',
        ];

        $category = PageCategory::all()->toArray();
        if (!empty($category)) {
            MakeTree::getData($category);
            $category_id = array(null => 'بدون دسته') + MakeTree::GenerateSelect();
        } else {
            $category_id = array(null => 'بدون دسته');
        }

        return View('admin.pages.edit')
            ->with('category_id', $category_id)
            ->with('data', $data)
            ->with('status', $status);

    }


    public function postEdit($id, PagesRequest $request)
    {
        $input = $request->all();
        $pages = Pages::find($id);

        if ($request->hasFile('image')) {
            $path = "assets/uploads/pages/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            File::delete($path . 'big/' . $pages->image);
            File::delete($path . 'medium/' . $pages->image);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        if ($request->has('category_id')) {
            $input['category_id'] = $request->get('category_id');
        } else {
            $input['category_id'] = null;
        }

        $input['show_side'] = $request->has('show_side');
        $input['show_first'] = $request->has('show_first');
        $input['show_menu'] = $request->has('show_menu');
        $pages->update($input);
        return Redirect::action('Admin\PagesController@getIndex')
            ->with('success', 'آیتم مورد نظر با موفقیت ویرابش شد.');
    }

    public function postDelete(PagesRequest $request)
    {
        $images = Pages::whereIn('id', $request->get('deleteId'))->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/pages/big/' . $item);
            File::delete('assets/uploads/pages/medium/' . $item);
        }
        if (Pages::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\PagesController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function postSort(PagesRequest $request)
    {
        if ($request->get('update') == "update") {
            $count = 1;
            if ($request->get('update') == 'update') {
                foreach ($request->get('arrayorder') as $idval) {
                    $category = Pages::find($idval);
                    $category->listorder = $count;
                    $category->save();
                    $count++;
                }
                echo 'با موفقیت ذخیره شد.';
            }

        }

    }
}
