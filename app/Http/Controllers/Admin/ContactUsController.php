<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ContactUsRequest;
use App\Models\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ContactUsController extends Controller
{
    public function getIndex(Request $request)
    {
        $query = ContactUs::query();

        if ($request->has('search')) {
            if ($request->get('start') != null and $request->get('end') != null) {
                $start = explode('/', $request->get('start'));
                $end = explode('/', $request->get('end'));

                $s = jmktime(0, 0, 0, $start[1], $start[0], $start[2]);
                $e = jmktime(0, 0, 0, $end[1], $end[0], $end[2]);

                $query->whereBetween('created_at', array($s, $e));
            }
        }
        $data = $query->latest()->paginate(15);


        return View('admin.contact-us.index')
            ->with('data', $data);

    }

    public function getEdit($id)
    {
        $contact_us = ContactUs::find($id);
        $contact_us->status = !$contact_us->status;
        $contact_us->save();
        return Redirect::back();
    }


    public function postDelete(ContactUsRequest $request)
    {
        if (ContactUs::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\ContactUsController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

}
