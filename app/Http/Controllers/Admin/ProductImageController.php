<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductImageRequest;
use App\Models\ProductImage;
use Classes\UploadImg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ProductImageController extends Controller
{
    public function getList($product_id)
    {
        $data = ProductImage::whereProductId($product_id)->latest()->paginate(15);
        return View('admin.product.images')
            ->with('product_id', $product_id)
            ->with('data', $data);
    }


    public function postAdd(ProductImageRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image')) {
            $path = "assets/uploads/product/images/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if ($fileName) {
                $input['image'] = $fileName;
                if ($request->has('water_mark')) {
                    $uploader->waterMark($fileName, $path);
                }
            } else {
                return Redirect::back()->with('error', 'عکس ارسالی صحیح نیست.');
            }
        }

        $productImage = ProductImage::create($input);

        return Redirect::back()->with('success', 'آیتم جدید اضافه شد.');
    }

    public function postDelete(ProductImageRequest $request)
    {
        $images = ProductImage::whereIn('id', $request->get('deleteId'))->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/product/images/big/' . $item);
            File::delete('assets/uploads/product/images/medium/' . $item);
        }
        if (ProductImage::destroy($request->get('deleteId'))) {
            return Redirect::back()->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }

    public function postSort(ProductImageRequest $request)
    {
        if ($request->get('update') == "update") {
            $count = 1;
            if ($request->get('update') == 'update') {
                foreach ($request->get('arrayorder') as $idval) {
                    $category = ProductImage::find($idval);
                    $category->listorder = $count;
                    $category->save();
                    $count++;
                }
                echo 'با موفقیت ذخیره شد.';
            }

        }

    }
}
