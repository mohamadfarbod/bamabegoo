<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageCategory extends Model
{
    protected $table = 'page_category';
    protected $fillable = [
        'title', 'parent_id', 'status', 'listorder'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }

    public function parent()
    {
        return $this->hasOne('App\Models\PageCategory', 'id', 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany('App\Models\PageCategory', 'parent_id', 'id');
    }

    public function pages()
    {
        return $this->hasMany('App\Models\Pages', 'category_id', 'id');
    }

}
