<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable = [
        'title', 'lead', 'content', 'image', 'keyword', 'description',
        'status', 'listorder', 'title_seo'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }
}
