<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTab extends Model
{
    protected $table = 'product_tab';
    protected $fillable = [
        'title', 'content', 'image', 'product_id', 'status', 'listorder'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }


}
