<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'setting';
    public $timestamps = false;
    protected $fillable = [
        'title', 'keyword', 'description', 'logo_water_mark',
        'favicon', 'logo_header', 'logo_footer', 'about_us',
        'contact_us', 'map', 'text1', 'text2'
    ];

}
