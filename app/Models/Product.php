<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = [
        'title', 'title_en', 'category_id', 'content', 'image', 'title_seo', 'keyword', 'description',
        'status', 'listorder', 'show_side', 'show_first', 'show_menu', 'lead', 'visit', 'vip',
        'version', 'android', 'update', 'builder', 'file', 'brand_id', 'under_slider', 'hot', 'alt'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id');
    }

    public function productImage()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id', 'id')->whereStatus(1)->latest();
    }

    public function productTab()
    {
        return $this->hasMany('App\Models\ProductTab', 'product_id', 'id')->whereStatus(1)->latest();
    }

    public function comment()
    {
        return $this->hasMany('App\Models\ProductComment', 'product_id')->whereStatus(1)->with('reply');
    }

}
