<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = [
        'title', 'parent_id', 'status', 'show_side', 'show_first', 'show_menu'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }

    public function parent()
    {
        return $this->hasOne('App\Models\Category', 'id', 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    public function product()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id')->whereStatus(1)->latest();
    }


    public function productFirst()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id')
            ->whereStatus(1)
            ->whereShowFirst(1)
            ->orderByRaw("RAND()")
            ->latest();
    }

    public function productMenu()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id')
            ->whereStatus(1)
            ->whereShowMenu(1)
            ->orderByRaw("RAND()")
            ->latest();
    }

}
