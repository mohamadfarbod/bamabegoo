<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductLike extends Model
{
    protected $table = 'product_like';
    protected $fillable = [
        'title', 'ip', 'product_id', 'vote'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }


}
