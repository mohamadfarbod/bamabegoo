<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = 'pages';
    protected $fillable = [
        'title', 'category_id', 'content', 'image', 'keyword', 'description',
        'status', 'listorder', 'show_menu', 'title_seo'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }

    public function category()
    {
        return $this->belongsTo('App\Models\PageCategory', 'category_id');
    }
}
