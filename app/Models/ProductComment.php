<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{
    protected $table = 'product_comments';
    protected $fillable = [
        'name', 'email', 'content', 'reply', 'product_id', 'status',
    ];

    protected function getDateFormat()
    {
        return 'U';
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function replySite()
    {
        return $this->hasMany('App\Models\ProductCommentReply', 'product_comment_id')->with('user')->whereStatus(1)->latest();
    }


    public function reply()
    {
        return $this->hasMany('App\Models\ProductCommentReply', 'product_comment_id')->with('user')->latest();
    }

}
