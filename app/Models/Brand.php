<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    protected $fillable = [
        'title', 'status', 'show_side', 'show_first', 'show_menu'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }

    public function product()
    {
        return $this->hasMany('App\Models\Product', 'brand_id', 'id')->whereStatus(1)->latest();
    }


    public function productFirst()
    {
        return $this->hasMany('App\Models\Product', 'brand_id', 'id')
            ->whereStatus(1)
            ->whereShowFirst(1)
            ->orderByRaw("RAND()")
            ->latest();
    }

    public function productMenu()
    {
        return $this->hasMany('App\Models\Product', 'brand_id', 'id')
            ->whereStatus(1)
            ->whereShowMenu(1)
            ->orderByRaw("RAND()")
            ->latest();
    }

}
