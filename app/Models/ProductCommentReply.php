<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCommentReply extends Model
{
    protected $table = 'product_comments_reply';
    protected $fillable = [
        'product_comment_id', 'user_id', 'content', 'name', 'email', 'status'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comment()
    {
        return $this->belongsTo('App\Models\ProductComment', 'product_comment_id');
    }
}
