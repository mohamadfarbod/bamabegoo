<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Uploader extends Model
{
    protected $table = 'uploader';
    protected $fillable = [
        'title', 'file'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }
}
