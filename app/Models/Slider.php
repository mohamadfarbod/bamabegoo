<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'slider';
    protected $fillable = [
        'title', 'image', 'status', 'listorder', 'type', 'place', 'link'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }

    public function scopeSlider($query)
    {
        return $query->wherePlace(0);
    }

    public function scopeBanner1($query)
    {
        return $query->wherePlace(1);
    }

    public function scopeBanner2($query)
    {
        return $query->wherePlace(2);
    }

}
