<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $table = 'social';
    protected $fillable = [
        'title', 'link', 'icon', 'status'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }
}
