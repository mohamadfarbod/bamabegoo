<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_image';
    protected $fillable = [
        'title', 'image', 'product_id', 'status', 'listorder'
    ];

    protected function getDateFormat()
    {
        return 'U';
    }


}
