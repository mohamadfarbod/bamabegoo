<?php

use App\Models\Brand;
use App\Models\Category;
use App\Models\ContactUs;
use App\Models\PageCategory;
use App\Models\Pages;
use App\Models\Setting;
use App\Models\Social;
use Illuminate\Support\Facades\Schema;

$setting = [];

if (Schema::hasTable('setting'))
    $setting = Setting::first();

View::composer('layouts.admin.blocks.sidebar', function ($view) {
    $count['contact_us'] = ContactUs::whereStatus(0)->count();
    $view->with('count', $count);
});

View::composer('layouts.site.blocks.header', function ($view) {
    $category = Category::whereStatus(1)
        ->whereShowMenu(1)
        ->whereNull('parent_id')
        ->orderby('listorder', 'ASC')
        ->get();

    $brand = Brand::whereStatus(1)
        ->whereShowMenu(1)
        ->orderby('listorder', 'ASC')
        ->get();

    $view->with('category', $category)->with('brand', $brand);
});


View::composer('layouts.site.blocks.footer', function ($view) {

    $social = Social::whereStatus(1)
        ->get();

    $page_category = PageCategory::whereStatus(1)
        ->whereNull('parent_id')
        ->orderby('listorder', 'ASC')
        ->get();

    $pages = Pages::whereStatus(1)
        ->whereNull('category_id')
        ->orderby('listorder', 'ASC')
        ->get();

    $category = Category::whereStatus(1)
        ->whereShowMenu(1)
        ->whereNull('parent_id')
        ->orderby('listorder', 'ASC')
        ->get();

    $view->with('pages', $pages)
        ->with('page_category', $page_category)
        ->with('category', $category)
        ->with('social', $social);
});


View::share([
    'setting' => $setting,
]);
