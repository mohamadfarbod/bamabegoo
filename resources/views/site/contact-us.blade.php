@extends ("layouts.site.master")
@section('content')

	<div class="container" style="padding: 0" >
		<div class="col-md-12 box-title inner-page">
			<h4>تماس با ما</h4>
		</div>
		<div class="col-md-12" style="background-color: #fff; padding: 20px">
			<div class="col-md-6 " style="padding: 40px 20px">
				{!!$setting->contact_us!!}
			</div>
			<div class="col-md-6 ">
				<form role="form" action="{{URL::action('Site\HomeController@postContactUs')}}" method="POST" id="rahweb_form">
					{{ csrf_field() }}
					<h2>ارسال پیام</h2>
					<div class="form-group">
						<input placeholder="نام" id="name" name="name" type="text" class="form-control">
					</div>
					<div class="form-group">
						<input placeholder="موضوع" id="title" name="title" type="text" class="form-control">
					</div>
					<div class="form-group">
						<input placeholder="ایمیل" id="email" name="email" type="email" class="form-control">
					</div>
					<div class="form-group">
						<textarea id="content" name="content" class="form-control yekan" placeholder="متن نظر"></textarea>
					</div>
					<input type="submit" class="btn btn-success send" value="ارسال نظر">
				</form>
			</div>
		</div>
	</div>

@stop
@section('css')
<style>
	.error{color:red;  }
</style>
@stop
@section('js')
	<script>
	(function($,W,D)
	{var JQUERY4U = {};
		JQUERY4U.UTIL =
		{
			setupFormValidation: function()
			{
				$("#rahweb_form").validate({
					rules: {
						name: "required",
						email: {
							required: true,
							email: true
						},
						content: "required",
						agree: "required"
					},
					messages: {
						name: "این فیلد الزامی است.",
						email: "لطفا یک آدرس ایمیل معتبر وارد کنید.",
						content: "این فیلد الزامی است."
					},
					submitHandler: function(form) {
						form.submit();
					}
				});
			}
		}
		$(D).ready(function($) {
			JQUERY4U.UTIL.setupFormValidation();
		});
	})(jQuery, window, document);
</script>		
@endsection