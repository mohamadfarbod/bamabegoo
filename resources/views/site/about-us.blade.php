@extends ("layouts.site.master")
@section('content')
	<div class="container" style="padding: 0" >
		<div class="col-md-12 box-title inner-page">
			<h4>درباره ما</h4>
		</div>
		<div class="col-md-12" style="background-color: #fff; padding: 20px">
			{!!$setting->about_us!!}
		</div>
	</div>
@stop
