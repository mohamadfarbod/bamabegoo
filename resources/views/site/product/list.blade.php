@extends("layouts.site.master")
@section('content')
	<div class="container" style="padding: 0" >
	<div class="col-md-12 box-title inner-page">
		<img src="{!! asset('assets/site/img/01.png') !!}" width="2%">
		<h4> {{$category->title}} </h4>
	</div>
	<div class="row" style="min-height: 380px">
		@foreach($products as $product)
			@include('layouts.site.blocks.product-box')
		@endforeach
	</div>
	</div>
	<center>
		@if(count($products))
			{!! $products->appends(Request::except('page'))->render() !!}
		@endif
	</center>
@stop


