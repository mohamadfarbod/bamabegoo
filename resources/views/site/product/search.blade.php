@extends("layouts.site.master")
@section('content')
	<div class="row">
		<div class="col-md-12 box-title">
			<img src="{!! asset('assets/site/img/01.png') !!}" width="2%">
			<h4> نتیجه جستجو {{$search}} </h4>
		</div>
	</div>
	<div class="row">
		@foreach($products as $product)
			@include('layouts.site.blocks.product-box')
		@endforeach
	</div>
	<center>
		@if(count($products))
			{!! $products->appends(Request::except('page'))->render() !!}
		@endif
	</center>
@stop



