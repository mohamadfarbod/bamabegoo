@extends("layouts.site.master")
@section('content')

	<div class="row">
		<div class="col-md-12 box-title">
			<img src="{!! asset('assets/site/img/01.png') !!}" width="2%">
			<h4> {{$category->title}} </h4>
		</div>
	</div>
	<div class="row">
		@foreach($categoryList as $item)
			<div class="col-md-4 col-sm-12 box" style="padding:0 0 0 2px ">
				<figure class="snip1581">
					<img src="{{ asset('assets/uploads/setting/main/'.$setting->logo_header)}}" alt="{{$item->title}}" width="100%">
				</figure>
				<div class="title">
					<a href="{{URL::action('Site\ProductController@getList',[$item->id])}}"><h6>{{$item->title}}</h6></a>
				</div>
			</div>
		@endforeach
	</div>
	
	
	<center>
		@if(count($categoryList))
			{!! $categoryList->appends(Request::except('page'))->render() !!}
		@endif
	</center>	
		
@stop

