@extends ("layouts.site.master")
@if($product->title_seo == '')
    @section('title',$product->title)
@else
    @section('title',$product->title_seo)
@endif
@section('keyword',$product->keyword)
@section('description',$product->description)
@section('content')
    <div class="container" style="padding: 0;">
        <div class="col-md-12 box-title inner-page">
            <h1>{{$product->title}}</h1>
        </div>
        <div class="row ">
            <div class="col-md-8 slid">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach($product->productImage as $key=>$item)
                            <li data-target="#myCarousel" data-slide-to="{{$key}}"
                                class="@if($key==0) active @endif"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach($product->productImage as $key=>$item)
                            <div class="item @if($key==0) active @endif">
                                <img src="@if(file_exists('assets/uploads/product/images/big/'.$item->image))
                                {!! asset('assets/uploads/product/images/big/'.$item->image) !!}
                                @else
                                {!! asset('assets/uploads/notFound.jpg') !!}
                                @endif" alt="{{$product->title}}" style="width:100%;">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                @foreach($banner as $row)
                    @php
                        if(file_exists('assets/uploads/slider/big/'.$row->image)){
                            $src = asset('assets/uploads/slider/big/'.$row->image);
                        }else{ $src = asset('assets/uploads/notFound.jpg');}
                    @endphp
                    <a href="{{$row->link}}">
                        <img src="{{$src}}" alt="{{$row->title}}" width="100%">
                    </a>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="container-fluid des-box">
                    <img src="@if(file_exists('assets/uploads/product/big/'.$product->image))
                    {!! asset('assets/uploads/product/big/'.$product->image) !!}
                    @else
                    {!! asset('assets/uploads/notFound.jpg') !!}
                    @endif" alt="{{$product->alt}}" width="100%">
                    <div class="btns pull-left">
                        <a class="btn btn-danger " href="https://plus.google.com/share?url={{URL::current()}}"> گوگل پلاس  <i class="fa fa-google-plus"></i></a>
                        <a class="btn btn-info " target="_blank"
                           href="https://telegram.me/share/url?url={{URL::current()}}">  تلگرام <i class="fa fa-paper-plane"></i></a>
                        <a class="btn btn-primary "
                           href="https://www.facebook.com/sharer/sharer.php?u={{URL::current()}}"> اینستاگرام <i class="fa fa-instagram "></i></a>
                    </div>
                    <br>
                    <div class='col-md-12 panel-des'>
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default head">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#description">توضیحات</a>
                                    </h4>
                                </div>
                                <div id="description" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        {!!$product->content!!}
                                    </div>
                                </div>
                            </div>
                            @foreach($product->productTab as $key=>$item)
                                <div class="panel panel-default head">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion"
                                               href="#appinja{{$item->id}}">
                                                {{$item->title}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="appinja{{$item->id}}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            {!!$item->content!!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="panel panel-default head">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#comment">
                                            دیدگاه کاربران این مطلب
                                        </a>
                                    </h4>
                                </div>
                                <div id="comment" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <section class="comment-list">
                                                @foreach($product->comment as $comment)
                                                    <article class="row">
                                                        <div class="col-md-2 col-sm-2 hidden-xs">
                                                            <figure class="thumbnail">
                                                                <img class="img-responsive "
                                                                     src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg"/>
                                                            </figure>
                                                        </div>
                                                        <div class="col-md-10 col-sm-10">
                                                            <div class="panel panel-default arrow right">
                                                                <div class="panel-body">
                                                                    <header class="text-right">
                                                                        <div class="comment-user"><i
                                                                                    class="fa fa-user"></i>
                                                                            {{$comment->name}}
                                                                        </div>
                                                                        <time class="comment-date"
                                                                              datetime="16-12-2014 01:05">
                                                                            <i class="fa fa-clock-o"></i>
                                                                            {{jdate('d F Y',$comment->created_at->timestamp)}}
                                                                        </time>
                                                                    </header>
                                                                    <div class="comment-post">
                                                                        <p>
                                                                            {{$comment->content}}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                    <br>
                                                    @foreach($comment->replySite as $reply)
                                                        <article class="row">
                                                            <div class="col-md-2 col-sm-2 col-md-pull-1 hidden-xs">
                                                                <figure class="thumbnail">
                                                                    <img class="img-responsive"
                                                                         src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg"/>
                                                                </figure>
                                                            </div>
                                                            <div class="col-md-9 col-sm-9 col-md-offset-1 col-md-pull-1 col-sm-offset-0">
                                                                <div class="panel panel-default arrow right">
                                                                    <div class="panel-heading ">پاسخ</div>
                                                                    <div class="panel-body">
                                                                        <header class="text-right">
                                                                            <div class="comment-user">
                                                                                <i class="fa fa-user"></i>
                                                                                {{$reply->name}}
                                                                            </div>
                                                                            <time class="comment-date"
                                                                                  datetime="16-12-2014 01:05">
                                                                                <i class="fa fa-clock-o"></i>
                                                                                {{jdate('d F Y',$reply->created_at->timestamp)}}
                                                                            </time>
                                                                        </header>
                                                                        <div class="comment-post">
                                                                            <p>
                                                                                {{$reply->content}}
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    @endforeach
                                                @endforeach
                                            </section>

                                        </div>
                                        <hr>
                                        <div class="col-lg-12 message">
                                            <h4 style="color: #e93f1c">ارسال دیدگاه</h4>
                                            <hr>
                                            <form action="{{URL::action('Site\HomeController@postComment')}}"
                                                  method="POST" id="rahweb_form">
                                                {{ csrf_field() }}
                                                <input name="product_id" type="hidden" value="{{$product->id}}">
                                                <div class="form-group col-md-6">
                                                    <input type="text" name="name" placeholder="نام"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <input type="text" name="email" placeholder="ایمیل"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <textarea rows="3" name="content" class="form-control" name="text"
                                                              placeholder="متن پیام"></textarea>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <button class="btn btn-danger btn-lg pull-left" type="submit"
                                                            style="width: 180px">
                                                        ارسال
                                                        نظر
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 box-news">
                <div class="container-fluid title">
                    <div style=" padding-right: 10px;">
                        <h3 style="font-size: 18px">مطالب مرتبط </h3>
                    </div>
                    <hr>
                    @foreach($productRel as $row)
                        <div class="container-fluid box-in">
                            <div class="col-md-4 ">
                                <img src="@if(file_exists('assets/uploads/product/big/'.$row->image))
                                {!! asset('assets/uploads/product/big/'.$row->image) !!}
                                @else
                                {!! asset('assets/uploads/notFound.jpg') !!}
                                @endif" alt="{{$row->alt}}" width="100%">
                            </div>
                            <div class="col-md-8 text">
                                <a href="{{URL::action('Site\ProductController@getDetails',$row->id)}}">
                                    <p>{{$row->title}}</p>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
@stop
@section('css')
    <style>
        .error { color: red;  }
    </style>
@stop
@section('js')
    <script>
        (function ($, W, D) {
            var JQUERY4U = {};
            JQUERY4U.UTIL =
                {setupFormValidation: function () {
                        $("#rahweb_form").validate({
                            rules: {
                                name: "required",
                                email: {
                                    required: true,
                                    email: true
                                },
                                content: "required",
                                agree: "required"
                            },
                            messages: {
                                name: "این فیلد الزامی است.",
                                email: "لطفا یک آدرس ایمیل معتبر وارد کنید.",
                                content: "این فیلد الزامی است."
                            },
                            submitHandler: function (form) {
                                form.submit();
                            }
                        });
                    }
                }
            $(D).ready(function ($) {
                JQUERY4U.UTIL.setupFormValidation();
            });
        })(jQuery, window, document);
    </script>
@endsection