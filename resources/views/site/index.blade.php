@extends("layouts.site.master")
@section('content')
	<div class="container">
@include('layouts.site.blocks.slider')
        <div class="row">
			@if(count($product1))
				<div class="col-md-8 center-block" style="padding:0 0 0 5px;margin: 2px 0">
					<div class="section-box-ten">
						<figure>
						<a href="{{URL::action('Site\ProductController@getDetails',[$product1->id])}}">
							<h4>{{$product1->title}}</h4></a>
							<p>{{$product1->lid}}</p>
						</figure>
						<img src="@if(file_exists('assets/uploads/product/medium/'.$product1->image))
								{!! asset('assets/uploads/product/medium/'.$product1->image) !!}
								@else
								{!! asset('assets/uploads/notFound.jpg') !!}
								@endif " alt="{{$product1->alt}}" class="img-responsive" width="100%"/>
					</div>
				</div>
			 @endif
			@if(count($product2))
				<div class="col-md-4 col-sm-12 col-xs-12 block-left" style="padding: 0;margin-top:-3px">
					@foreach($product2 as $product)
						<div style="margin: 5px 0">
							<div class="section-box-ten">
								<figure>
									<a href="{{URL::action('Site\ProductController@getDetails',[$product->id])}}"><h6>{{$product->title}}</h6></a>
									<p>{{$product->lid}}</p>
								</figure>
								<img  src="@if(file_exists('assets/uploads/product/medium/'.$product->image))
									{!! asset('assets/uploads/product/medium/'.$product->image) !!}
									@else
									{!! asset('assets/uploads/notFound.jpg') !!}
									@endif " alt="{{$product->alt}}" class="img-responsive" width="100%"/>
							</div>
						</div>
					@endforeach
				</div>
			@endif
        </div>
		<div class="row">
			<div class="col-md-12 box-title">
				<img src="{!! asset('assets/site/img/01.png') !!}" width="2%">
				<h4> پیشنهاد ما</h4>
			</div>
		</div>
		<div class="row">
			@foreach($productVip as $product)
				<a href="{{URL::action('Site\ProductController@getDetails',[$product->id])}}">
					<div class="col-md-4 col-sm-12 box" style="padding:0 2px 0 2px ">
						<figure class="snip1581">
							<img src="@if(file_exists('assets/uploads/product/medium/'.$product->image))
							{!! asset('assets/uploads/product/medium/'.$product->image) !!}
							@else
							{!! asset('assets/uploads/notFound.jpg') !!}
							@endif" alt="{{$product->alt}}" width="100%">
						</figure>
						<div class="title" >
							<h6>{{$product->title}}</h6>
						</div>
					</div>
				</a>
			@endforeach
		</div>
		@foreach($banner as $row)
			@php
				if(file_exists('assets/uploads/slider/big/'.$row->image)){
					$src = asset('assets/uploads/slider/big/'.$row->image);
				}else{$src = asset('assets/uploads/notFound.jpg');}
			@endphp
			<div class="row" style="margin-top: 5px ">
				<a href="{{$row->link}}">
					<img src="{{$src}}" alt="{{$row->title}}" width="100%">
				</a>
			</div>
		@endforeach
		<div class="row">
            <div class="col-md-8 col-sm-12" style="margin-top: 5px">
				@foreach($category as $row)
					@if(count($row->productFirst))
						 <div class="row">
							<div class="col-md-12 box-title">
								<img src="{!! asset('assets/site/img/01.png') !!}" width="3%">
								<a href="{{URL::action('Site\ProductController@getList',[$row->id])}}"><h4> {{$row->title}} </h4></a>
							</div>
							@foreach($row->productFirst as $product)
								<a href="{{URL::action('Site\ProductController@getDetails',[$product->id])}}">
									<div class="col-md-6 col-sm-12 box " style="padding:2px 2px 0 2px ">
										<div class="section-box-ten">
											<figure>
												<a href="{{URL::action('Site\ProductController@getDetails',[$product->id])}}"><h6>{{$product->title}}</h6></a>
												<p>{{ $product->lead }}</p>

											</figure>
											<img src="@if(file_exists('assets/uploads/product/big/'.$product->image))
											{!! asset('assets/uploads/product/big/'.$product->image) !!}
											@else
											{!! asset('assets/uploads/notFound.jpg') !!}
											@endif " alt="{{$product->alt}}" class="img-responsive" width="100%"/>
										</div>
									</div>
								</a>
							@endforeach
						</div>
					@endif
				@endforeach
			</div>
            <div class="col-md-4 col-sm-12 box-news" style="padding-left: 0">
                <div class=" title">
                    <div>
                        <img src="{!! asset('assets/site/img/01.png') !!}" width="7%">
                        <h4>داغ ترین مطالب </h4>
                    </div>
					<br>
					@foreach($productHot as $product)
						<div class="container-fluid box-in">
							<div class="col-md-4 ">
								<img src="@if(file_exists('assets/uploads/product/medium/'.$product->image))
									{!! asset('assets/uploads/product/medium/'.$product->image) !!}
									@else
									{!! asset('assets/uploads/notFound.jpg') !!}
									@endif " alt="{{$product->alt}}" width="100%">
							</div>
							<div class="col-md-8 text">
								<a href="{{URL::action('Site\ProductController@getDetails',[$product->id])}}">
									<p>{{$product->title}}</p>
								</a>
							</div>
						</div>
					@endforeach
                </div>
            </div>
        </div>
		<div class="row">
			<h3 style="text-align: center;color: #5e5e5e">تیم پشتیبانی</h3>
			<hr>
			<div class="col-md-3">
				<img src="http://keenthemes.com/assets/bootsnipp/member1.png" alt="" class="img-circle" width="100%">
				<div style="text-align: center">
					<h4>علی میرحسینی</h4>
					<p>تخصص : مشاور خانواده</p>
				</div>
			</div>
			<div class="col-md-3">
				<img src="http://keenthemes.com/assets/bootsnipp/member2.png" alt="" class="img-circle" width="100%">
				<div style="text-align: center">
					<h4>علی میرحسینی</h4>
					<p>تخصص : مشاور خانواده</p>
				</div>
			</div>
			<div class="col-md-3">
				<img src="http://keenthemes.com/assets/bootsnipp/member1.png" alt="" class="img-circle" width="100%">
				<div style="text-align: center">
					<h4>علی میرحسینی</h4>
					<p>تخصص : مشاور خانواده</p>
				</div>
			</div>
			<div class="col-md-3">
				<img src="http://keenthemes.com/assets/bootsnipp/member2.png" alt="" class="img-circle" width="100%">
				<div style="text-align: center">
					<h4>علی میرحسینی</h4>
					<p>تخصص : مشاور خانواده</p>
				</div>
			</div>
		</div>
		<br>
</div>
@stop
