@extends ("layouts.admin.master")
@section('title','نظرها')
@section('part','نظرها')
@section('content')
	<div class="row">
		<div class="col-md-12">
		  <!-- general form elements -->
		  <div class="box box-primary">
			<div class="box-header with-border">
			  <h3 class="box-title">پاسخ</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			
				@include('layouts.admin.blocks.message')
					<div class="chat">   
					  <div class="chat-history">
						<ul class="chat-ul">
						  <li>
							  <h4 style="text-align: right;direction: rtl;">
								{{@$data->product->title}}
							  </h4>
						  </li>
						  
							 <li>
								<div class="message-data  align-left">
								  <span class="message-data-name"><i class="fa fa-circle you"></i> 
								  
										{{$data->name}} 
									
								  - {{jdate('Y/m/d H:i',$data->created_at->timestamp)}}</span>
								</div>
								<div class="message you-message" style="text-align: right;direction: rtl;">
									{!! $data->content !!}
									
								</div>
							  </li>
						 
						 
							@if(count($data->reply))
								@foreach($data->reply as $item)
									@if($item->user_id == Auth::id())
											<li class="clearfix">
												<div class="message-data align-right">
												  <span class="message-data-name">
													@if(count($item->user))
															{{$item->user->name.' '.$item->user->family}} 
														@else
															{{$item->name}} 
														@endif
													  - {{jdate('Y/m/d H:i',$item->created_at->timestamp)}}
													  </span> <i class="fa fa-circle me"></i>
												</div>
												<div class="message me-message float-right" style="text-align: right;direction: rtl;">
													{!! $item->content !!}
													&nbsp;&nbsp;&nbsp;
															<a data-toggle="tooltip" data-original-title=" @if($item->status) تایید شده @else  تایید نشده @endif" 
															href="{{URL::action('Admin\ProductCommentController@getActiveReply',[$item->id])}}"
															   class="btn btn-{{ ($item->status) ? 'success' : 'danger' }} btn-xs"><span
																	class="fa fa-{{ ($item->status) ? 'check' : 'times' }}"></span>
															</a>
												</div>
											  </li>
											  
										@else
											<li>
												<div class="message-data align-left">
												  <span class="message-data-name"><i class="fa fa-circle you"></i> 

													@if(count($item->user))
														{{$item->user->name.' '.$item->user->family}} 
													@else
														{{$item->name}} 
													@endif
												  - {{jdate('Y/m/d H:i',$item->created_at->timestamp)}}</span>
												</div>
												<div class="message you-message" style="text-align: right;direction: rtl;">
													{!! $item->content !!}
													
														&nbsp;&nbsp;&nbsp;
														<a data-toggle="tooltip" data-original-title=" @if($item->status) تایید شده @else  تایید نشده @endif" 
														href="{{URL::action('Admin\ProductCommentController@getActiveReply',[$item->id])}}"
														   class="btn btn-{{ ($item->status) ? 'success' : 'danger' }} btn-xs"><span
																class="fa fa-{{ ($item->status) ? 'check' : 'times' }}"></span>
														</a>
												</div>
											</li>
											@endif
									@endforeach
								@endif
							</ul>
						
					  </div> <!-- end chat-history -->
					  
					</div> <!-- end chat -->
				<form method="post" action="{{URL::action('Admin\ProductCommentController@postReply',$data->id)}}" enctype="multipart/form-data" id="rahweb_form">
				{{ csrf_field() }}
					<div class="box-body">
							<div class="form-group">
								<div class="row">

									<div class="col-md-12">
										<label>پاسخ:</label>
										<textarea class="form-control" rows="3" id="content" name="content" 
													placeholder="توضیحات را وارد کنید . . ."></textarea>
									</div>
								</div>
							</div>
							
							<div class="col-md-4 ejavan_col">
								<button type="submit" class="btn btn-primary">پاسخ</button>
							</div>
					</div>
				</form>
														
		  </div><!-- /.box -->
		</div>
	</div>
@stop

@section('css')
    <link href="{{ asset('assets/admin/css/chat.css')}}" rel="stylesheet">
@stop
	
@section('js')

    <script>
	(function($,W,D)
	{
		var JQUERY4U = {};

		JQUERY4U.UTIL =
		{
			setupFormValidation: function()
			{
				//form validation rules
				$("#rahweb_form").validate({
					rules: {
						content: "required",
						agree: "required"
					},
					messages: {
						content: "این فیلد الزامی است."
					},
					submitHandler: function(form) {
						form.submit();
					}
				});
			}
		}

		//when the dom has loaded setup form validation rules
		$(D).ready(function($) {
			JQUERY4U.UTIL.setupFormValidation();
		});

	})(jQuery, window, document);
</script>		
@endsection