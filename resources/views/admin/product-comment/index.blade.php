@extends ("layouts.admin.master")
@section('title','نظرات محصولات')
@section('part','نظرات محصولات')
@section('content')
	<div class="row">
		@include('layouts.admin.blocks.message')
		<div class="col-xs-12">
		  <div class="box">
			<div class="box-header">
			  <h3 class="box-title"></h3>
			  <form method="post" action="{{URL::action('Admin\ProductCommentController@postDelete')}}" style="float: left">
				{{ csrf_field() }}
			  <div class="box-tools">
			  
				<a href="#" data-toggle="modal" data-target="#search"  class="btn btn-info btn-xs">
					<i class="fa fa-search"></i> جستجو
				</a>
				
				<button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
				data-toggle="tooltip"
								   data-original-title="حذف موارد انتخابی"
					class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
				</button>
				
				
			  </div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
			  <table class="table table-hover">
				<tr>
					<th>
						<center>
							<input id="check-all" style="opacity: 1;position:static;" type="checkbox"/>
						</center>
					</th>
					<th>نام</th>
					<th>مربوط به</th>
					<th>ایمیل</th>
					<th>تاریخ</th>
					<th>وضعیت</th>
					<th>عملیات</th>
				</tr>
				@foreach($data as $row)

					<tr>
						<td>
							<center>
								<input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
									   type="checkbox"
									   value="{{$row->id}}"/>

							</center>
						</td>
						<td>{{$row->name}}</td>
						<td>{{@$row->product->title}}</td>
						<td>{{$row->email}}</td>
						<td>{{jdate('Y/m/d H:i',$row->created_at->timestamp)}}</td>

						<td>
							<center>
								@if($row->status==1)
									<span class='label label-success'>فعال</span>
								@else
									<span class='label label-danger'>غیر فعال</span>
								@endif
							</center>
						</td>
						<td>
							<center>
								<a data-toggle="tooltip" data-original-title="{{ ($row->status) ? 'لغو تایید' : 'تایید' }}" 
									href="{{URL::action('Admin\ProductCommentController@getEdit',[$row->id])}}"
									   class="btn btn-{{ ($row->status) ? 'success' : 'danger' }} btn-xs"><span
											class="fa fa-{{ ($row->status) ? 'check' : 'times' }}"></span>
									</a>
									<a href="#" class="btn btn-info  btn-xs edit" data-toggle="modal"
										data-target="#messageModal{{ $row->id }}" id="{{ $row->id }}">
										<i class="fa fa-eye"></i> مشاهده پیام
									</a>
									<a href="{{URL::action('Admin\ProductCommentController@getReply',[$row->id])}}" data-toggle="tooltip"
									   data-original-title="ویرایش" class="btn btn-warning  btn-xs edit"><i
										class="fa fa-edit"></i> پاسخ</a>
							</center>
						</td>
					</tr>
					
					<div class="modal fade" id="messageModal{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
												aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="messageModalLabel" style="padding-right: 40px;">{{ @$row->product->title }} - {{ $row->product_id }}</h4>
								</div>
								<div class="modal-body" style="text-align: justify;">
								<div class="form-title" style="font-size: 13px;">ارسال کننده: {{ $row->name }}  <span style="float: left;">  تاریخ:{{jdate('y/m/d H:i',$row->created_at->timestamp)}}</span></div>
								</br>
								{{$row->content}}
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
								</div>
							</div>
						</div>
					</div>
	
				@endforeach
				
			  </table>
			  </form>
				<center>
					@if(count($data))
						{!! $data->appends(Request::except('page'))->render() !!}
					@endif
				</center>							
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div>						
	  </div>

	  
	  
<div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
	 <form method="'GET" action="{{URL::current()}}"class="form-horizontal">
	 {{ csrf_field() }}
	<input type="hidden" name="search" value="search">
    <div class="modal-dialog" style="direction: rtl;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="messageModalLabel"
                    style="direction: rtl; text-align: right; padding-right: 20px;"><i class="fa fa-search"></i> جستجو
                </h4>
            </div>
            <div class="modal-body" style="text-align: justify;">
                <div class="widget flat radius-bordered">
                    <div class="widget-body">
                        <div id="registration-form">

                            <div class="form-group">
								<label class="col-lg-3 control-label">از تاریخ:</label>
                                <div class="col-lg-9">
									<input class="form-control date" type="text" id="start" name="start" placeholder="از تاریخ">
                                </div>
                            </div>

                            <div class="form-group">
								<label class="col-lg-3 control-label">تا تاریخ:</label>
                                <div class="col-lg-9">
									<input class="form-control date" type="text" id="end" name="end" placeholder="تا تاریخ">
                                </div>
                            </div>
                            <div class="form-group">
								<label class="col-lg-3 control-label">دسته:</label>
                                <div class="col-lg-9">
									<select name="category_id" id="category_id" class="form-control">
										@foreach($category as $key=>$item)
											<option value="{{$key}}" @if(isset($data->category_id) and $data->category_id==$key) selected @endif>{{$item}}</option>
										@endforeach
									</select>
                                </div>
                            </div>
                            <div class="form-group">
								<label class="col-lg-3 control-label">مربوط به:</label>
                                <div class="col-lg-9">
									<center class="loading">
										<img src="{{ asset('assets/admin/img/loading.gif')}}" width="30" height="30"/>
										در حال بارگذاری لطفا شکیبا باشید
									</center>
									<select name="product_id" id="product_id" class="form-control">
										
										
									</select>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <button type="submit" data-toggle="tooltip" data-original-title="جستجو" class="btn btn-blue">جستجو
                </button>
            </div>
        </div>
    </div>
    </form>
</div>


@stop
@section('css')
    <link href="{{ asset('assets/admin/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@stop


@section('js')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.fa.min.js')}}"></script>

    <script>

        $(".date").datepicker({
            changeMonth: true,
            changeYear: true,
            isRTL: true
        });
		
        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
			
			$('.loading').hide();
		
			var product_id = $("#product_id");
			$("#category_id").on('change', function () {
				$('.loading').show();
				$('#product_id').hide();
				var category_id = $(this).val();
				$('.btn-primary').attr('disabled', 'disabled');
				$.ajax({
					url: "{!! URL::action('Admin\ProductCommentController@postAjax') !!}",
					data: {
						_token: "{!! csrf_token() !!}",
						key: category_id
					},
					method: 'POST',
					success: function (result) {
						$('.loading').hide();
						$('#product_id').show();
						var data = jQuery.parseJSON(result);
						if (data.status === true) {
							product_id.empty();
							product_id.append(data.value);
							$('.btn-info').removeAttr('disabled');
						} else {
							alert(data.error);
						}
					}
				});
			});
		
        });
    </script>


@stop