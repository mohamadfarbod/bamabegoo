{{ csrf_field() }}
<div class="box-body">

	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>عنوان:</label>
				<input class="form-control" type="text" id="title" name="title" 
					value="@if(isset($data->title)) {{$data->title}} @endif" 
					placeholder="عنوان  را وارد کنید . . .">
			</div>
			
			<div class="col-md-6">
				<label>وضعیت:</label>
				<select name="status" id="status" class="form-control">
					@foreach($status as $key=>$item)
						<option value="{{$key}}" @if(isset($data->status) and $data->status==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
			
		
			
		</div>
	</div>
	
	<div class="form-group">
		<div class="row">
				<div class="col-md-2">
				<input type="checkbox" name="show_first" value="1" @if(isset($data) and $data->show_first) checked="checked" @endif>
				<span style="margin: 2px;"> صفحه اول </span>
			</div>
			
			<div class="col-md-2">
				<input type="checkbox" name="show_menu" value="1" @if(isset($data) and $data->show_menu) checked="checked" @endif>
				<span style="margin: 2px;"> منو </span>
			</div>
			<div class="col-md-2">
				<input type="checkbox" name="show_side" value="1" @if(isset($data) and $data->show_side) checked="checked" @endif>
				<span style="margin: 2px;"> سایدبار </span>
			</div>
		</div>
	</div>
	
	
  <div class="box-footer">
	<button type="submit" class="btn btn-primary">ذخیره</button>
  </div>