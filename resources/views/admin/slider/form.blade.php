{{ csrf_field() }}
<div class="box-body">
	<div class="form-group">
		<label>عنوان:</label>
		<input class="form-control" type="text" id="title" name="title" 
			value="@if(isset($data->title)) {{$data->title}} @endif" 
			placeholder="عنوان  را وارد کنید . . .">
	</div>
	
	<div class="box-body">
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>جایگاه:</label>
				<select name="place" id="place" class="form-control">
					@foreach($place as $key=>$item)
						<option value="{{$key}}" @if(isset($data->place) and $data->place==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-6">
				<label>وضعیت:</label>
				<select name="status" id="status" class="form-control">
					@foreach($status as $key=>$item)
						<option value="{{$key}}" @if(isset($data->status) and $data->status==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	
	
	<div class="form-group">
		<label>انتخاب عکس:</label>
		<div class="row">
			<div class="col-md-4">
				<input type="file" name="image" id="imgInp" class="form-control"/>
			</div>
			<div class="col-md-2">
				@php
					$image = "notFound";
					if(isset($data)) $image = $data->image;
				@endphp
				<img src="@if(file_exists('assets/uploads/slider/medium/'.$image)) 
							{!! asset('assets/uploads/slider/medium/'.$image) !!}
						@else {!! asset('assets/uploads/notFound.jpg') !!} @endif" 
					class="img-rounded" id="blah"
					style="width: 100px; height: 60px;">
               
			</div>
			
			<div class="col-md-2">
				<input type="checkbox" name="water_mark" value="1">
				<span style="margin: 2px;"> واتر مارک </span>
			</div>
		</div>
	</div>
	
  <div class="box-footer">
	<button type="submit" class="btn btn-primary">ذخیره</button>
  </div>