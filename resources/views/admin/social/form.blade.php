{{ csrf_field() }}
<div class="box-body">
	<div class="form-group">
		<label>عنوان:</label>
		<input class="form-control" type="text" id="title" name="title" 
			value="@if(isset($data->title)) {{$data->title}} @endif" 
			placeholder="عنوان  را وارد کنید . . .">
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>لینک:</label>
				<input class="form-control" type="text" id="link" name="link" 
					value="@if(isset($data->link)) {{$data->link}} @endif" 
					placeholder="لینک  را وارد کنید . . .">
			</div>
			<div class="col-md-6">
				<label>وضعیت:</label>
				<select name="status" id="status" class="form-control">
					@foreach($status as $key=>$item)
						<option value="{{$key}}" @if(isset($data->status) and $data->status==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	
	
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>ایکن:</label>
				<input class="form-control" type="text" id="icon" name="icon" 
					value="@if(isset($data->icon)) {{$data->icon}} @endif" 
					placeholder="ایکن  را وارد کنید . . .">
			</div>
			<div class="col-md-6 rahweb_col">
				<div class="alert alert-info fade in">
					برای انتخاب آیکون   
					<a style="font-size: large; color: rgb(54, 32, 162);" href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank"> اینجا </a>
					کلیک کنید.
				</div>
			</div>
		</div>
	</div>
	
	
  <div class="box-footer">
	<button type="submit" class="btn btn-primary">ذخیره</button>
  </div>
