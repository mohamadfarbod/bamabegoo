@extends ("layouts.admin.master")
@section('title','جوامع مجازی')
@section('part','جوامع مجازی')
@section('content')
	<div class="row">
		<div class="col-md-12">
		  <!-- general form elements -->
		  <div class="box box-primary">
			<div class="box-header with-border">
			  <h3 class="box-title">ویرایش</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			
				@include('layouts.admin.blocks.message')
				<form method="post" action="{{URL::action('Admin\SocialController@postEdit',$data->id)}}" enctype="multipart/form-data" id="rahweb_form">
					@include('admin.social.form')
				</form>
														
		  </div><!-- /.box -->
		</div>
	</div>
@stop


	
@section('css')
	<link href="{{ asset('assets/admin/css/bootstrap-select.min.css')}}" rel="stylesheet">
@stop


@section('js')
	<script src="{{ asset('assets/admin/js/bootstrap-select.min.js')}}"></script>
	


    <script>
	(function($,W,D)
	{
		var JQUERY4U = {};

		JQUERY4U.UTIL =
		{
			setupFormValidation: function()
			{
				//form validation rules
				$("#rahweb_form").validate({
					rules: {
						title: "required",
						icon: "required",
						link: "required",
						agree: "required"
					},
					messages: {
						title: "این فیلد الزامی است.",
						icon: "این فیلد الزامی است.",
						link: "این فیلد الزامی است."
					},
					submitHandler: function(form) {
						form.submit();
					}
				});
			}
		}

		//when the dom has loaded setup form validation rules
		$(D).ready(function($) {
			JQUERY4U.UTIL.setupFormValidation();
		});

	})(jQuery, window, document);
</script>		
@endsection