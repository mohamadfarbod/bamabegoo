{{ csrf_field() }}
<div class="box-body">
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>عنوان:</label>
				<input class="form-control" type="text" id="name" name="name" 
					value="@if(isset($data->name)) {{$data->name}} @endif" 
					placeholder="عنوان  را وارد کنید . . .">
			</div>
			<div class="col-md-6" style="margin-top: 25px;">
				<input type="checkbox" name="select_all" value="1" id="select_all">
					<span class="text">انتخاب همه</span>
			</div>
		</div>
	</div>
	
	<hr>
			<div class="form-group">
				<?php 
					if(isset($data->permission)){
						$accessDB = unserialize($data->permission);
					}else{
						$accessDB = [];
					}
				?>
				@foreach(Config::get('site.permisions') as $key=>$value)
					<div class="widget col-md-6" style="background-color: rgb(248, 248, 248); border: medium solid;">
						<div class="widget-header bordered-bottom bordered-themesecondary">
							<i class="widget-icon fa fa-unlock-alt themesecondary"></i>
							<span class="widget-caption themesecondary" style="color: #3c8dbc;">{{{ $value['title'] }}}</span>
						</div>
						<!--Widget Header-->
						<div class="widget-body">
							<div class="widget-main no-padding">
								<div class="tickets-container" style="height: 150px;">
									@foreach($value['access'] as $keyAccess => $access)
										<?php
											$check = 0;
											if(isset($accessDB[$key][$keyAccess])) {
												$check = 1;
											}
										?>
										<div class="col-md-6">
											<input  type="checkbox"  name="access[{{$key}}][{{$keyAccess}}]" value="1" @if($check) checked @endif>
											<span class="text">{{{ $access }}}</span>
											
										</div>
									@endforeach
									
									
									<div class="checkbox" style="display: none;">
										<label style="padding-left: 0px;">
											<input  type="checkbox"  name="access[user][changePassword]" value="1" checked>
										</label>
									</div>
									
									<br/>
								</div>

							</div>
						</div>
					</div>
				@endforeach
			</div>
									
	
	
  <div class="box-footer">
	<button type="submit" class="btn btn-primary">ذخیره</button>
  </div>