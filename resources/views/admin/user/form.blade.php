{{ csrf_field() }}
<div class="box-body">
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>نام:</label>
				<input class="form-control" type="text" id="name" name="name" 
					value="@if(isset($data->name)) {{$data->name}} @endif" 
					placeholder="نام  را وارد کنید . . .">
			</div>
			<div class="col-md-6">
				<label>وضعیت:</label>
				<select name="status" id="status" class="form-control">
					@foreach($status as $key=>$item)
						<option value="{{$key}}" @if(isset($data->status) and $data->status==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label>ایمیل:</label>
		<input class="form-control" type="email" id="email" name="email" 
			value="@if(isset($data->email)) {{$data->email}} @endif" 
			placeholder="ایمیل  را وارد کنید . . .">
	</div>
	
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>رمز:</label>
					<input class="form-control" type="password" id="password" name="password" placeholder="رمز عبور">
			</div>
			<div class="col-md-6">
				<label>تکرار رمز:</label>
					<input class="form-control" type="password" id="repassword" name="repassword" placeholder="تکرار رمز عبور">
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label>گروه کاربری:</label>
		<div class="row">
			@foreach($groups as $group)
				<div class="col-md-6">
				</div>
				<div class="col-md-6">
					<input type="checkbox" name="group[]" value="{{$group->id}}" @if(in_array($group->id, $groupsId)) checked="checked" @endif>
					
					<span class="text">{{ $group->name }}</span>
			
				</div>
			@endforeach
		</div>
	</div>
	
  <div class="box-footer">
	<button type="submit" class="btn btn-primary">ذخیره</button>
  </div>