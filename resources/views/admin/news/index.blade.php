@extends ("layouts.admin.master")
@section('title','اخبار')
@section('part','اخبار')
@section('content')
	<div class="row">
		@include('layouts.admin.blocks.message')
		<div class="col-xs-12">
		  <div class="box">
			<div class="box-header">
			  <h3 class="box-title"></h3>
			  <form method="post" action="{{URL::action('Admin\NewsController@postDelete')}}" style="float: left">
				{{ csrf_field() }}
			  <div class="box-tools">
			  
				<a href="#" data-toggle="modal" data-target="#search"  class="btn btn-info btn-xs">
					<i class="fa fa-search"></i> جستجو
				</a>
				
				<a class="btn btn-success btn-xs" href="{{URL::action('Admin\NewsController@getAdd')}}" data-toggle="tooltip"
								   data-original-title="آیتم جدید">
					<i class="fa fa-plus"></i> جدید 
				</a>
				
				<button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
				data-toggle="tooltip"
								   data-original-title="حذف موارد انتخابی"
					class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
				</button>
				
				
			  </div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
			  <table class="table table-hover">
				<tr>
					<th>
						<center>
							<input id="check-all" style="opacity: 1;position:static;" type="checkbox"/>
						</center>
					</th>
					<th>عنوان</th>
					<th>دسته</th>
					<th>تصویر</th>
					<th>وضعیت</th>
					<th>عملیات</th>
				</tr>
				@foreach($data as $row)
					<tr>
						<td>
							<center>
								<input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
									   type="checkbox"
									   value="{{$row->id}}"/>

							</center>
						</td>
						<td>{{$row->title}}</td>
						<td>{{@$row->category->title}}</td>
						<td>
							@if(file_exists('assets/uploads/news/medium/'.$row->image))
								<img src="{!! asset('assets/uploads/news/medium/'.$row->image) !!}" 
									class="img-rounded"
									style="width: 100px; height: 60px;">
							@else
								<img src="{!! asset('assets/uploads/notFound.jpg') !!}" 
									class="img-rounded"
									style="width: 100px; height: 60px;">
							@endif
						</td>
						
						<td>
							<center>
								@if($row->status==1)
									<span class='label label-success'>فعال</span>
								@else
									<span class='label label-danger'>غیر فعال</span>
								@endif
							</center>
						</td>
						<td>
							<center>
								<a href="{{URL::action('Admin\NewsController@getEdit',$row->id)}}" data-toggle="tooltip"
								   data-original-title="ویرایش اطلاعات" class="btn btn-warning  btn-xs"><i
											class="fa fa-edit"></i> ویرایش </a>
							</center>
						</td>
					</tr>

				@endforeach
				
			  </table>
			  </form>
				<center>
					@if(count($data))
						{!! $data->appends(Request::except('page'))->render() !!}
					@endif
				</center>							
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div>							
	  </div>

	  
	  
<div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
	 <form method="GET" action="{{URL::current()}}"class="form-horizontal">
	 {{ csrf_field() }}
	<input type="hidden" name="search" value="search">
    <div class="modal-dialog" style="direction: rtl;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="messageModalLabel"
                    style="direction: rtl; text-align: right; padding-right: 20px;"><i class="fa fa-search"></i> جستجو
                </h4>
            </div>
            <div class="modal-body" style="text-align: justify;">
                <div class="widget flat radius-bordered">
                    <div class="widget-body">
                        <div id="registration-form">

                            <div class="form-group">
								<label class="col-lg-3 control-label">از تاریخ:</label>
                                <div class="col-lg-9">
									<input class="form-control date" type="text" id="start" name="start" placeholder="از تاریخ">
                                </div>
                            </div>

                            <div class="form-group">
								<label class="col-lg-3 control-label">تا تاریخ:</label>
                                <div class="col-lg-9">
									<input class="form-control date" type="text" id="end" name="end" placeholder="تا تاریخ">
                                </div>
                            </div>
							
                            <div class="form-group">
								<label class="col-lg-3 control-label">عنوان:</label>
                                <div class="col-lg-9">
									<input class="form-control" type="text" id="title" name="title">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <button type="submit" data-toggle="tooltip" data-original-title="جستجو" class="btn btn-blue">جستجو
                </button>
            </div>
        </div>
    </div>
    </form>
</div>


@stop
@section('css')
    <link href="{{ asset('assets/admin/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@stop


@section('js')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.fa.min.js')}}"></script>

    <script>

        $(".date").datepicker({
            changeMonth: true,
            changeYear: true,
            isRTL: true
        });
		
        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>


@stop