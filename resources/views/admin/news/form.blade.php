{{ csrf_field() }}
<div class="box-body">
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<label>عنوان:</label>
				<input class="form-control" type="text" id="title" name="title" 
					value="@if(isset($data->title)) {{$data->title}} @endif" 
					placeholder="عنوان  را وارد کنید . . .">
			</div>
			<div class="col-md-6">
				<label>وضعیت:</label>
				<select name="status" id="status" class="form-control">
					@foreach($status as $key=>$item)
						<option value="{{$key}}" @if(isset($data->status) and $data->status==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label>انتخاب عکس:</label>
		<div class="row">
			<div class="col-md-4">
				<input type="file" name="image" id="imgInp" class="form-control"/>
			</div>
			<div class="col-md-2">
				@php
					$image = "notFound";
					if(isset($data)) $image = $data->image;
				@endphp
				<img src="@if(file_exists('assets/uploads/news/medium/'.$image)) 
							{!! asset('assets/uploads/news/medium/'.$image) !!}
						@else {!! asset('assets/uploads/notFound.jpg') !!} @endif" 
					class="img-rounded" id="blah"
					style="width: 100px; height: 60px;">
               
			</div>
			
			<div class="col-md-2">
				<input type="checkbox" name="water_mark" value="1">
				<span style="margin: 2px;"> واتر مارک </span>
			</div>
		</div>
	</div>
	
	
	<div class="form-group">
		<label>لید:</label>
		<textarea class="form-control" id="lead" name="lead" 
				placeholder="لید وارد کنید . . .">@if(isset($data->lead)) {{$data->lead}} @endif</textarea>
	</div>
	
	
	<div class="form-group">
		<label>توضیحات:</label>
		<textarea class="form-control ckeditor" id="content" name="content" 
				placeholder="توضیحات را وارد کنید . . .">@if(isset($data->content)) {{$data->content}} @endif</textarea>
	</div>
	
	<div class="form-group">
		<label>عنوان سئو:</label>
		<input class="form-control" type="text" id="title_seo" name="title_seo" 
			value="@if(isset($data->title_seo)) {{$data->title_seo}} @endif" 
			placeholder="عنوان سئو را وارد کنید . . .">
	</div>
	
	<div class="form-group">
		<label>کلمات کلیدی:</label>
		<input class="form-control" type="text" id="keyword" name="keyword" 
			value="@if(isset($data->keyword)) {{$data->keyword}} @endif" 
			placeholder="کلمات با | جدا شوند. موسیقی|آموزش">
	</div>
	
	<div class="form-group">
		<label>توضیحات سئو:</label>
		<textarea class="form-control" id="description" name="description" 
				placeholder="توضیحات سئو را وارد کنید . . .">@if(isset($data->description)) {{$data->description}} @endif</textarea>
	</div>
	
	
  <div class="box-footer">
	<button type="submit" class="btn btn-primary">ذخیره</button>
  </div>
