{{ csrf_field() }}
<div class="box-body">
	<div class="form-group">
		<div class="row">
			<div class="col-md-4">
				<label>عنوان:</label>
				<input class="form-control" type="text" id="title" name="title" 
					value="@if(isset($data->title)) {{$data->title}} @endif" 
					placeholder="عنوان  را وارد کنید . . .">
			</div>
			<div class="col-md-4">
				<label>عنوان انگلیسی:</label>
				<input class="form-control" type="text" id="title_en" name="title_en" 
					value="@if(isset($data->title_en)) {{$data->title_en}} @endif" 
					placeholder="عنوان انگلیسی  را وارد کنید . . .">
			</div>
			<div class="col-md-4">
				<label>alt:</label>
				<input class="form-control" type="text" id="alt" name="alt" 
					value="@if(isset($data->alt)) {{$data->alt}} @endif" 
					placeholder="alt  را وارد کنید . . .">
			</div>
			
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>شماره نسخه:</label>
				<input class="form-control" type="text" id="version" name="version" 
					value="@if(isset($data->version)) {{$data->version}} @endif" 
					placeholder="شماره نسخه  را وارد کنید . . .">
			</div>
			<div class="col-md-6">
				<label>نسخه اندروید مورد نیاز:</label>
				<input class="form-control" type="text" id="android" name="android" 
					value="@if(isset($data->android)) {{$data->android}} @endif" 
					placeholder="نسخه اندروید  را وارد کنید . . .">
			</div>
			
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>سازنده:</label>
				<input class="form-control" type="text" id="builder" name="builder" 
					value="@if(isset($data->builder)) {{$data->builder}} @endif" 
					placeholder="سازنده را وارد کنید . . .">
			</div>
			<div class="col-md-6">
				<label>تاریخ به روز رسانی:</label>
				<input class="form-control date" type="text" id="update" name="update" 
				
					placeholder="تاریخ به روز رسانی  را وارد کنید . . .">
			</div>
			
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-4">
				<label>دسته:</label>
				<select name="category_id" id="category_id" class="form-control">
					@foreach($category_id as $key=>$item)
						<option value="{{$key}}" @if(isset($data->category_id) and $data->category_id==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-4">
				<label>دسته دوم:</label>
				<select name="brand_id" id="brand_id" class="form-control">
					@foreach($brand as $key=>$item)
						<option value="{{$key}}" @if(isset($data->brand_id) and $data->brand_id==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-4">
				<label>وضعیت:</label>
				<select name="status" id="status" class="form-control">
					@foreach($status as $key=>$item)
						<option value="{{$key}}" @if(isset($data->status) and $data->status==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label>انتخاب عکس:</label>
		<div class="row">
			<div class="col-md-4">
				<input type="file" name="image" id="imgInp" class="form-control"/>
			</div>
			<div class="col-md-2">
				@php
					$image = "notFound";
					if(isset($data)) $image = $data->image;
				@endphp
				<img src="@if(file_exists('assets/uploads/product/medium/'.$image)) 
							{!! asset('assets/uploads/product/medium/'.$image) !!}
						@else {!! asset('assets/uploads/notFound.jpg') !!} @endif" 
					class="img-rounded" id="blah"
					style="width: 100px; height: 60px;">
               
			</div>
			
			<div class="col-md-2">
				<input type="checkbox" name="show_first" value="1" @if(isset($data) and $data->show_first) checked="checked" @endif>
				<span style="margin: 2px;"> صفحه اول </span>
			</div>
			
			<div class="col-md-2">
				<input type="checkbox" name="under_slider" value="1" @if(isset($data) and $data->under_slider) checked="checked" @endif>
				<span style="margin: 2px;"> زیر اسلایدر </span>
			</div>
			<div class="col-md-2">
				<input type="checkbox" name="hot" value="1" @if(isset($data) and $data->hot) checked="checked" @endif>
				<span style="margin: 2px;"> داغ ترین </span>
			</div>
			<div class="col-md-2">
				<input type="checkbox" name="vip" value="1" @if(isset($data) and $data->vip) checked="checked" @endif>
				<span style="margin: 2px;"> پیشنهاد ویژه </span>
			</div>
			
			<div class="col-md-2">
				<input type="checkbox" name="water_mark" value="1">
				<span style="margin: 2px;"> واتر مارک </span>
			</div>
		</div>
	</div>
	
	
	<div class="form-group">
		<label>انتخاب فایل:</label>
		<div class="row">
			<div class="col-md-4">
				<input type="file" name="file" class="form-control"/>
			</div>
			<div class="col-md-2">
				@php
					$file = "notFound";
					if(isset($data)) $file = $data->file;
				@endphp
				@if(file_exists('assets/uploads/product/file/'.$image))
					<a href="{!! asset('assets/uploads/product/file/'.$file) !!}"><i calss="fa fa-donload"></i> دانلود </a>
				@endif					
				
               
			</div>
		</div>
	</div>
	
	
	<div class="form-group">
		<label>لید:</label>
		<textarea class="form-control" id="lead" name="lead" 
				placeholder="لید وارد کنید . . .">@if(isset($data->lead)) {{$data->lead}} @endif</textarea>
	</div>
	
	
	<div class="form-group">
		<label>توضیحات:</label>
		<textarea class="form-control ckeditor" id="content" name="content" 
				placeholder="توضیحات را وارد کنید . . .">@if(isset($data->content)) {{$data->content}} @endif</textarea>
	</div>
	
	<div class="form-group">
		<label>عنوان سئو:</label>
		<input class="form-control" type="text" id="title_seo" name="title_seo" 
			value="@if(isset($data->title_seo)) {{$data->title_seo}} @endif" 
			placeholder="عنوان سئو را وارد کنید . . .">
	</div>
	
	<div class="form-group">
		<label>کلمات کلیدی:</label>
		<input class="form-control" type="text" id="keyword" name="keyword" 
			value="@if(isset($data->keyword)) {{$data->keyword}} @endif" 
			placeholder="کلمات با | جدا شوند. موسیقی|آموزش">
	</div>
	
	<div class="form-group">
		<label>توضیحات سئو:</label>
		<textarea class="form-control" id="description" name="description" 
				placeholder="توضیحات سئو را وارد کنید . . .">@if(isset($data->description)) {{$data->description}} @endif</textarea>
	</div>
	
	
  <div class="box-footer">
	<button type="submit" class="btn btn-primary">ذخیره</button>
  </div>
