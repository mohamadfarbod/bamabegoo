@extends ("layouts.admin.master")
@section('title','محصولات')
@section('part','محصولات')
@section('content')
	<div class="row">
		@include('layouts.admin.blocks.message')
		<div class="col-xs-8">
		  <div class="box">
			<div class="box-header">
			  <h3 class="box-title"></h3>
			  <form method="post" action="{{URL::action('Admin\ProductController@postDelete')}}" style="float: left">
				{{ csrf_field() }}
			  <div class="box-tools">
			  
				<a href="#" data-toggle="modal" data-target="#search"  class="btn btn-info btn-xs">
					<i class="fa fa-search"></i> جستجو
				</a>
				
				<a class="btn btn-success btn-xs" href="{{URL::action('Admin\ProductController@getAdd')}}" data-toggle="tooltip"
								   data-original-title="آیتم جدید">
					<i class="fa fa-plus"></i> جدید 
				</a>
				
				<button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
				data-toggle="tooltip"
								   data-original-title="حذف موارد انتخابی"
					class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
				</button>
				
				
			  </div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
			  <table class="table table-hover">
				<tr>
					<th>
						<center>
							<input id="check-all" style="opacity: 1;position:static;" type="checkbox"/>
						</center>
					</th>
					<th>عنوان</th>
					<th>دسته</th>
					<th>دسته دوم</th>
					<th>تصویر</th>
					<th>وضعیت</th>
					<th>عملیات</th>
				</tr>
				@foreach($data as $row)
					<tr>
						<td>
							<center>
								<input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
									   type="checkbox"
									   value="{{$row->id}}"/>

							</center>
						</td>
						<td>{{$row->title}}</td>
						<td>{{@$row->category->title}}</td>
						<td>{{@$row->brand->title}}</td>
						<td>
							@if(file_exists('assets/uploads/product/medium/'.$row->image))
								<img src="{!! asset('assets/uploads/product/medium/'.$row->image) !!}" 
									class="img-rounded"
									style="width: 100px; height: 60px;">
							@else
								<img src="{!! asset('assets/uploads/notFound.jpg') !!}" 
									class="img-rounded"
									style="width: 100px; height: 60px;">
							@endif
						</td>
						
						<td>
							<center>
								@if($row->status==1)
									<span class='label label-success'>فعال</span>
								@else
									<span class='label label-danger'>غیر فعال</span>
								@endif
							</center>
						</td>
						<td>
							<center>
								<a href="{{URL::action('Admin\ProductController@getEdit',$row->id)}}" data-toggle="tooltip"
								   data-original-title="ویرایش اطلاعات" class="btn btn-warning  btn-xs"><i
											class="fa fa-edit"></i> ویرایش </a>
								<a href="{{URL::action('Admin\ProductImageController@getList',$row->id)}}" data-toggle="tooltip"
								   data-original-title="تصاویر بیشتر" class="btn btn-info  btn-xs"><i
											class="fa fa-picture-o"></i> تصاویر بیشتر </a>
								<a href="{{URL::action('Admin\ProductTabController@getList',$row->id)}}" data-toggle="tooltip"
								   data-original-title="تب ها" class="btn btn-info  btn-xs"><i
											class="fa fa-list"></i> تب ها </a>
							</center>
						</td>
					</tr>

				@endforeach
				
			  </table>
			  </form>
				<center>
					@if(count($data))
						{!! $data->appends(Request::except('page'))->render() !!}
					@endif
				</center>							
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div>
		
		<div class="col-xs-4">
			<div id="list">
				<div class="alert alert-info alert-dismissable" style="direction: rtl; margin: 0px auto;">
					<i class="fa fa-check"></i>
					@if($setCat)
					<span style="font-size: 14px;">	با درگ کردن ترتیب مورد نظر را انتخاب نمایید.  </span>
					@else
						<span style="font-size: 14px;">	برای مرتب سازی ابتدا دسته مورد نظر را انتخاب نمایید. </span>
					@endif
				</div>

				<div id="response"></div>
				<ul>
					@foreach($all as $rowSort)
				   
						<li id="arrayorder_{!! stripslashes($rowSort['id']) !!}">{!! stripslashes($rowSort['title']) !!}
							<div class="clear"></div>
						</li>
						
					@endforeach
				</ul>
			</div>
		</div>
										
	  </div>

	  
	  
<div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
	 <form method="GET" action="{{URL::current()}}"class="form-horizontal">
	 {{ csrf_field() }}
	<input type="hidden" name="search" value="search">
    <div class="modal-dialog" style="direction: rtl;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="messageModalLabel"
                    style="direction: rtl; text-align: right; padding-right: 20px;"><i class="fa fa-search"></i> جستجو
                </h4>
            </div>
            <div class="modal-body" style="text-align: justify;">
                <div class="widget flat radius-bordered">
                    <div class="widget-body">
                        <div id="registration-form">

                            <div class="form-group">
								<label class="col-lg-3 control-label">از تاریخ:</label>
                                <div class="col-lg-9">
									<input class="form-control date" type="text" id="start" name="start" placeholder="از تاریخ">
                                </div>
                            </div>

                            <div class="form-group">
								<label class="col-lg-3 control-label">تا تاریخ:</label>
                                <div class="col-lg-9">
									<input class="form-control date" type="text" id="end" name="end" placeholder="تا تاریخ">
                                </div>
                            </div>
							
                            <div class="form-group">
								<label class="col-lg-3 control-label">عنوان:</label>
                                <div class="col-lg-9">
									<input class="form-control" type="text" id="title" name="title">
                                </div>
                            </div>
                            <div class="form-group">
								<label class="col-lg-3 control-label">دسته:</label>
                                <div class="col-lg-9">
									<select name="category_id" id="category_id" class="form-control">
										@foreach($category as $key=>$item)
											<option value="{{$key}}">{{$item}}</option>
										@endforeach
									</select>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <button type="submit" data-toggle="tooltip" data-original-title="جستجو" class="btn btn-blue">جستجو
                </button>
            </div>
        </div>
    </div>
    </form>
</div>


@stop
@section('css')
    <style>
        ul {
            padding: 0px;
            margin: 0px;
        }

        #response {
            padding: 10px;
            background-color: #9F9;
            border: 2px solid #396;
            margin-bottom: 20px;
        }

        #list li {
            margin: 0 0 3px;
            padding: 8px;
            background-color: #333;
            color: #fff;
            list-style: none;
        }
    </style>

    <link href="{{ asset('assets/admin/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@stop


@section('js')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.fa.min.js')}}"></script>

    <script>

        $(".date").datepicker({
            changeMonth: true,
            changeYear: true,
            isRTL: true
        });
		
        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>
	
	<meta name="csrf-token" content="{!! csrf_token() !!}"/>
    <script type="text/javascript">
        $(document).ready(function () {
            function slideout() {
                setTimeout(function () {
                    $("#response").slideUp("slow", function () {
                    });

                }, 2000);
            }

            $("#response").hide();
            $(function () {
                $("#list ul").sortable({
                    opacity: 0.8, cursor: 'move', update: function () {
                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                        var order = $(this).sortable("serialize") + '&update=update' + '&_token=' + CSRF_TOKEN;
                        $.post("{!!URL::action('Admin\ProductController@postSort')!!} ", order, function (theResponse) {
                            $("#response").html(theResponse);
                            $("#response").slideDown('slow');
                            slideout();
                        });

                    }
                });
            });

        });
    </script>


@stop