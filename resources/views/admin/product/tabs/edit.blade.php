@extends ("layouts.admin.master")
@section('title','تب محصولات')
@section('part','تب محصولات')
@section('content')
	<div class="row">
		<div class="col-md-12">
		  <!-- general form elements -->
		  <div class="box box-primary">
			<div class="box-header with-border">
			  <h3 class="box-title">ویرایش</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			
				@include('layouts.admin.blocks.message')
				<form method="post" action="{{URL::action('Admin\ProductTabController@postEdit',$data->id)}}" enctype="multipart/form-data" id="rahweb_form">
					<input type="hidden" name="product_id" value="{{$data->product_id}}" />
					@include('admin.product.tabs.form')
				</form>
														
		  </div><!-- /.box -->
		</div>
	</div>
@stop



@section('css')
    <link href="{{ asset('assets/admin/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@stop


@section('js')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.fa.min.js')}}"></script>

    <script>

        $(".date").datepicker({
            changeMonth: true,
            changeYear: true,
            isRTL: true
        });
		
	(function($,W,D)
	{
		var JQUERY4U = {};

		JQUERY4U.UTIL =
		{
			setupFormValidation: function()
			{
				//form validation rules
				$("#rahweb_form").validate({
					rules: {
						title: "required",
						agree: "required"
					},
					messages: {
						title: "این فیلد الزامی است."
					},
					submitHandler: function(form) {
						form.submit();
					}
				});
			}
		}

		//when the dom has loaded setup form validation rules
		$(D).ready(function($) {
			JQUERY4U.UTIL.setupFormValidation();
		});

	})(jQuery, window, document);
</script>		
@endsection