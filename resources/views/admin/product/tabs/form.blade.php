{{ csrf_field() }}
<div class="box-body">
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>عنوان:</label>
				<input class="form-control" type="text" id="title" name="title" 
					value="@if(isset($data->title)) {{$data->title}} @endif" 
					placeholder="عنوان  را وارد کنید . . .">
			</div>
			<div class="col-md-6">
				<label>وضعیت:</label>
				<select name="status" id="status" class="form-control">
					@foreach($status as $key=>$item)
						<option value="{{$key}}" @if(isset($data->status) and $data->status==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label>توضیحات:</label>
		<textarea class="form-control ckeditor" id="content" name="content" 
				placeholder="توضیحات را وارد کنید . . .">@if(isset($data->content)) {{$data->content}} @endif</textarea>
	</div>
	
  <div class="box-footer">
	<button type="submit" class="btn btn-primary">ذخیره</button>
  </div>
