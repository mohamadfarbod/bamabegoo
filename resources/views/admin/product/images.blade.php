@extends ("layouts.admin.master")
@section('title','تصاویر بیشتر')
@section('part','تصاویر بیشتر')
@section('content')
    <div class="row">
        @include('layouts.admin.blocks.message')
        <div class="col-xs-12">
            <div class="box">
                <div class="box box-primary">
                    <!-- form start -->
					<form method="post" action="{{URL::action('Admin\ProductImageController@postAdd')}}" enctype="multipart/form-data" id="rahweb_form">
					{{ csrf_field() }}
					<input type="hidden" name="product_id" value="{{$product_id}}">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>عنوان:</label>
									<input class="form-control" type="text" id="title" name="title" 
										placeholder="عنوان  را وارد کنید . . .">
                                </div>
                                <div class="col-md-4">
                                    <label>تصویر :</label>
                                    <input class="form-control" type="file" id="image" name="image">
                                </div>
                                <div class="col-md-4 rahweb_col">
                                   <button type="submit" class="btn btn-primary">ذخیره</button>
                                </div>
                            </div>
                        </div>

                       </form>
                    </div><!-- /.box -->
                </div>


                <div class="box-header">
                    <h3 class="box-title">لیست تصاویر بیشتر</h3>
					<form method="post" action="{{URL::action('Admin\ProductImageController@postDelete')}}" style="float: left">
					{{ csrf_field() }}
                    <div class="box-tools">
                        <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
                                data-toggle="tooltip"
                                data-original-title="حذف موارد انتخابی"
                                class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
                        </button>


                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <center>
                                    <input id="check-all" style="opacity: 1;position:static;" type="checkbox"/>
                                </center>
                            </th>
                            <th>عنوان</th>
                            <th>تصویر</th>
                            <th>تاریخ ثبت</th>

                        </tr>
                        @foreach($data as $row)

                            <tr>
                                <td>
                                    <center>
                                        <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                               type="checkbox"
                                               value="{{$row['id']}}"/>

                                    </center>
                                </td>
                                <td>{{$row->title}}</td>
								<td>
									@if(file_exists('assets/uploads/product/images/medium/'.$row->image))
										<img src="{!! asset('assets/uploads/product/images/medium/'.$row->image) !!}" 
											class="img-rounded"
											style="width: 100px; height: 60px;">
									@else
										<img src="{!! asset('assets/uploads/notFound.jpg') !!}" 
											class="img-rounded"
											style="width: 100px; height: 60px;">
									@endif
								</td>
						
                                <td>{{jdate('Y/m/d',$row->created_at->timestamp)}}</td>


                            </tr>

                        @endforeach

                    </table>
                    </form>
                    <center>
                        @if(count($data))
                            {!! $data->appends(Request::except('page'))->render() !!}
                        @endif
                    </center>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>


@stop

@section('css')
    <link href="{{ asset('assets/admin/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@stop


@section('js')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.fa.min.js')}}"></script>

    <script>

        $(".date").datepicker({
            changeMonth: true,
            changeYear: true,
            isRTL: true
        });

    

        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
  
                (function($,W,D)
                {
                    var JQUERY4U = {};

                    JQUERY4U.UTIL =
                    {
                        setupFormValidation: function()
                        {
                            //form validation rules
                            $("#rahweb_form").validate({
                                rules: {
                                    title: "required",
                                    image: "required",

                                },
                                messages: {
                                    title: "این فیلد الزامی است.",
                                    image: "این فیلد الزامی است.",

                                },
                                submitHandler: function(form) {
                                    form.submit();
                                }
                            });
                        }
					}


                    //when the dom has loaded setup form validation rules
                    $(D).ready(function($) {
                        JQUERY4U.UTIL.setupFormValidation();
                    });

                })(jQuery, window, document);

            </script>

@stop