@extends ("layouts.admin.master")
	@section('content')
	
	 <?php  $x = 0; ?>
	
	<!-- Small boxes (Stat box) -->
	  <div class="row">
	  @include('layouts.admin.blocks.message')
		<div class="col-lg-3 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-aqua">
			<div class="inner">
			  <h3>{{$counts['user']}}</h3>
			  <p>کاربران فعال</p>
			</div>
			<div class="icon">
			  <i class="fa fa-user"></i>
			</div>
		  </div>
		</div><!-- ./col -->
		<div class="col-lg-3 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-green">
			<div class="inner">
			  <h3>{{$counts['page-category']}}</h3>
			  <p>دسته بندی صفحات ایستا</p>
			</div>
			<div class="icon">
			  <i class="fa fa-file"></i>
			</div>
		  </div>
		</div><!-- ./col -->
		<div class="col-lg-3 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-yellow">
			<div class="inner">
			  <h3>{{$counts['category']}}</h3>
			  <p>دسته بندی محصولات</p>
			</div>
			<div class="icon">
			  <i class="fa fa-list"></i>
			</div>
		  </div>
		</div><!-- ./col -->
		<div class="col-lg-3 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-red">
			<div class="inner">
			  <h3>{{$counts['product']}}</h3>
			  <p>محصولات</p>
			</div>
			<div class="icon">
			  <i class="fa fa-tags"></i>
			</div>
		  </div>
		</div><!-- ./col -->
	  </div><!-- /.row -->
	  
	  
	  
	   
	  <div class="col-xs-6">
		  <div class="box">
			<div class="box-header">
				<h3 class="box-title" style="color: #3c8dbc;">
					<i class="fa fa-comment" aria-hidden="true"></i>
					&nbsp;&nbsp;
					آخرین نظرات محصولات
				</h3>
			  <div class="box-tools">  </div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
			  <table class="table table-hover">
				<tr>
					<th>نام</th>
					<th>مربوط به</th>
					<th>متن نظر</th>
				</tr>
				
			@foreach($comments['product'] as $row)
				<tr>
					<td>{{$row->name}}</td>
					<td>{{@$row->product->title}}</td>
					<td>{{$row->content}}</td>
				</tr>
			@endforeach
			
			  </table>
			  
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div>

	  <div class="col-xs-6">
		  <div class="box">
			<div class="box-header">
				<h3 class="box-title" style="color: #3c8dbc;">
					<i class="fa fa-phone" aria-hidden="true"></i>
					&nbsp;&nbsp;
					آخرین پیام های تماس با ما
				</h3>
			  <div class="box-tools">  </div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
			  <table class="table table-hover">
				<tr>
					<th>نام</th>
					<th>موضوع</th>
					<th>متن پیام</th>
				</tr>
				
				
			@foreach($contact_us as $row)
				<tr>
					<td>{{$row->name}}</td>
					<td>{{$row->title}}</td>
					<td>{{$row->content}}</td>
				</tr>
			@endforeach
				
			  </table>
			  
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div>
		
	  <!-- Main row -->
	  <div class="row">
		
	  </div><!-- /.row (main row) -->
	@stop