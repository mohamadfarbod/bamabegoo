@extends ("layouts.admin.master")
@section('title','تنظیمات')
@section('part','تنظیمات ')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> ویرایش تنظیمات</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                @include('layouts.admin.blocks.message')
				<form method="post" action="{{URL::action('Admin\SettingController@postEdit')}}" enctype="multipart/form-data" id="rahweb_form">
					@include('admin.setting.form')
				</form>
            </div><!-- /.box -->
        </div>
    </div>
@stop



@section('js')

    <script>
        (function($,W,D)
        {
            var JQUERY4U = {};

            JQUERY4U.UTIL =
            {
                setupFormValidation: function()
                {
                    //form validation rules
                    $("#rahweb_form").validate({
                        rules: {
                            title: "required",


                        },
                        messages: {
                            title: "این فیلد الزامی است.",


                        },
                        submitHandler: function(form) {
                            form.submit();
                        }
                    });
                }
            }

            //when the dom has loaded setup form validation rules
            $(D).ready(function($) {
                JQUERY4U.UTIL.setupFormValidation();
            });

        })(jQuery, window, document);
		
		$("#logo_water_mark").change(function () {
			readURL(this, 'blah_logo_water_mark');
		});
		$("#favicon").change(function () {
			readURL(this, 'blah_favicon');
		});
		$("#logo_header").change(function () {
			readURL(this, 'blah_logo_header');
		});
		$("#logo_header_gallery").change(function () {
			readURL(this, 'blah_logo_header_gallery');
		});
		$("#logo_footer").change(function () {
			readURL(this, 'blah_logo_footer');
		});

    </script>
@endsection