{{ csrf_field() }}
	<ul class="nav nav-tabs " style="padding-right:0px">
		<li class="active">
			<a data-toggle="tab" href="#setting"> تنظیمات  </a>
		</li>

		<li>
			<a data-toggle="tab" href="#general"> عمومی 	</a>
		</li>

		<li>
			<a data-toggle="tab" href="#seo">سئو 	</a>
		</li>

	</ul>
	
<div class="tab-content">
	<div id="setting" class="tab-pane active">

		<div class="box-body">
			<div class="form-group">
				<div class="row">
					<div class="col-md-12">
						<label>عنوان:</label>
						<input class="form-control" type="text" id="title" name="title" 
							value="@if(isset($data->title)) {{$data->title}} @endif" 
							placeholder="عنوان  را وارد کنید . . .">
							
					</div>

				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<label>واتر مارک :</label>
						<input class="form-control" type="file" id="logo_water_mark" name="logo_water_mark">
					</div>
					<div class="col-md-2">
						@php
							$logo_water_mark = "notFound";
							if(isset($data)) $logo_water_mark = $data->logo_water_mark;
						@endphp
						<img src="@if(file_exists('assets/uploads/setting/main/'.$logo_water_mark)) 
									{!! asset('assets/uploads/setting/main/'.$logo_water_mark) !!}
								@else {!! asset('assets/uploads/notFound.jpg') !!} @endif" 
							class="img-rounded" id="blah_logo_water_mark"
							style="width: 100px; height: 60px;">
					   
					</div>
					<div class="col-md-4">
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<label>تصویر فوآیکون :</label>
						<input class="form-control" type="file" id="favicon" name="favicon">
					</div>
					<div class="col-md-2">
						@php
							$favicon = "notFound";
							if(isset($data)) $favicon = $data->favicon;
						@endphp
						<img src="@if(file_exists('assets/uploads/setting/main/'.$favicon)) 
									{!! asset('assets/uploads/setting/main/'.$favicon) !!}
								@else {!! asset('assets/uploads/notFound.jpg') !!} @endif" 
							class="img-rounded" id="blah_favicon"
							style="width: 100px; height: 60px;">
					   
					</div>
					<div class="col-md-4">
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<label>لوگو هدر :</label>
						<input class="form-control" type="file" id="logo_header" name="logo_header">
					</div>
					<div class="col-md-2">
						@php
							$logo_header = "notFound";
							if(isset($data)) $logo_header = $data->logo_header;
						@endphp
						<img src="@if(file_exists('assets/uploads/setting/main/'.$logo_header)) 
									{!! asset('assets/uploads/setting/main/'.$logo_header) !!}
								@else {!! asset('assets/uploads/notFound.jpg') !!} @endif" 
							class="img-rounded" id="blah_logo_header"
							style="width: 100px; height: 60px;">
					   
					</div>
					<div class="col-md-4">
					</div>
				</div>
			</div>



			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<label>لوگو هدر ویترین:</label>
						<input class="form-control" type="file" id="logo_header_gallery" name="logo_header_gallery">
					</div>
					<div class="col-md-2">
						@php
							$logo_header_gallery = "notFound";
							if(isset($data)) $logo_header_gallery = $data->logo_header_gallery;
						@endphp
						<img src="@if(file_exists('assets/uploads/setting/main/'.$logo_header_gallery)) 
									{!! asset('assets/uploads/setting/main/'.$logo_header_gallery) !!}
								@else {!! asset('assets/uploads/notFound.jpg') !!} @endif" 
							class="img-rounded" id="blah_logo_header_gallery"
							style="width: 100px; height: 60px;">
					   
					</div>
					<div class="col-md-4">
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<label>لوگو فوتر :</label>
						<input class="form-control" type="file" id="logo_footer" name="logo_footer">
					</div>
					<div class="col-md-2">
						@php
							$logo_footer = "notFound";
							if(isset($data)) $logo_footer = $data->logo_footer;
						@endphp
						<img src="@if(file_exists('assets/uploads/setting/main/'.$logo_footer)) 
									{!! asset('assets/uploads/setting/main/'.$logo_footer) !!}
								@else {!! asset('assets/uploads/notFound.jpg') !!} @endif" 
							class="img-rounded" id="blah_logo_footer"
							style="width: 100px; height: 60px;">
					   
					</div>
					<div class="col-md-4">
					</div>
				</div>
			</div>
		</div>
	</div>


		<div id="general" class="tab-pane">

			<div class="box-body">
				<div class="form-group">
					<label>درباره ما:</label>
					<textarea class="form-control ckeditor" id="about_us" name="about_us" 
							placeholder="درباره ما را وارد کنید . . .">@if(isset($data->about_us)) {{$data->about_us}} @endif</textarea>
				
				</div>
					<label>درباره ما فوتر:</label>
					<textarea class="form-control ckeditor" id="text2" name="text2" 
							placeholder="درباره ما را وارد کنید . . .">@if(isset($data->text2)) {{$data->text2}} @endif</textarea>
				
				</div>


				<div class="form-group">
					<label>تماس با ما:</label>
					<textarea class="form-control ckeditor" id="contact_us" name="contact_us" 
							placeholder="تماس با ما را وارد کنید . . .">@if(isset($data->contact_us)) {{$data->contact_us}} @endif</textarea>
				
				</div>
				<div class="form-group">
					<label>تماس با ما فوتر:</label>
					<textarea class="form-control ckeditor" id="text1" name="text1" 
							placeholder="تماس با ما فوتر را وارد کنید . . .">@if(isset($data->text1)) {{$data->text1}} @endif</textarea>
				
				</div>
				<div class="form-group">
					<label>map:</label>
					<textarea class="form-control" id="map" name="map" >@if(isset($data->map)) {{$data->map}} @endif</textarea>
				
				</div>

			</div>
		</div>

		<div id="seo" class="tab-pane">

			<div class="box-body">
				<div class="form-group">
					<label>کلمات کلیدی:</label>
					<input class="form-control" type="text" id="keyword" name="keyword" 
						value="@if(isset($data->keyword)) {{$data->keyword}} @endif" 
						placeholder="کلمات کلیدی  را وارد کنید . . .">
				</div>


				<div class="form-group">
					<label>توضیحات کلیدی:</label>
					<textarea class="form-control" id="description" name="description" 
							placeholder="توضیحات کلیدی را وارد کنید . . .">@if(isset($data->description)) {{$data->description}} @endif</textarea>
				
				</div>

			</div>
		</div>
		
	</div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">ذخیره</button>
    </div>
