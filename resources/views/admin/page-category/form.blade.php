{{ csrf_field() }}
<div class="box-body">

	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<label>عنوان:</label>
				<input class="form-control" type="text" id="title" name="title" 
					value="@if(isset($data->title)) {{$data->title}} @endif" 
					placeholder="عنوان  را وارد کنید . . .">
			</div>
			<?php /*
			<div class="col-md-2">
			</br>
				<input type="checkbox" name="side_left" value="1" @if(isset($data) and $data->side_left) checked="checked" @endif>
				<span style="margin: 2px;"> سایدبار چپ </span>
			</div>
			
			<div class="col-md-2">
				<input type="checkbox" name="side_right" value="1" @if(isset($data) and $data->side_right) checked="checked" @endif>
				<span style="margin: 2px;"> سایدبار راست </span>
			</div>
			*/ ?>
		</div>
	</div>
	
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>دسته:</label>
				<select name="parent_id" id="parent_id" class="form-control">
					@foreach($parent_id as $key=>$item)
						<option value="{{$key}}" @if(isset($data->parent_id) and $data->parent_id==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-6">
				<label>وضعیت:</label>
				<select name="status" id="status" class="form-control">
					@foreach($status as $key=>$item)
						<option value="{{$key}}" @if(isset($data->status) and $data->status==$key) selected @endif>{{$item}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	
	
  <div class="box-footer">
	<button type="submit" class="btn btn-primary">ذخیره</button>
  </div>