<!DOCTYPE html>
<html lang="fa">
@include('layouts.site.blocks.head')
<body>
<div class="row">
	@include('layouts.site.blocks.header')
@yield('content')
	@include('layouts.site.blocks.footer')
	@include('layouts.site.blocks.script')
</body>
</html>