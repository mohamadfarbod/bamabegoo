 <div class="container-fluid footer">
        <div class="container">
            <div class="col-md-2 section">
                <h4>دسته بندی</h4><br>
				@foreach($category as $row)
					<p><a href="{{URL::action('Site\ProductController@getList',[$row->id])}}">{{$row->title}}</a></p>
				@endforeach	
            </div>
            <div class="col-md-5">
                <h4> درباره ما</h4><br>
                <p>{!!$setting->text2!!}</p>
            </div>
            <div class="col-md-3">
                <h4>اطلاعات تماس</h4><br>
                <p>{!!$setting->text1!!}</p>
                <p>
					@foreach($social as $item)
						<a href="{{$item->link}}"><i class="fa fa-{{$item->icon}}"></i></a>
					@endforeach
                </p>
            </div>
            <div class="col-md-2 section">
                <h4>دسترسی سریع</h4><br>
                <p><a href="{{URL::action('Site\HomeController@getIndex')}}">خانه</a></p>
                <p><a href="{{URL::action('Site\HomeController@getContactUs')}}">تماس با ما</a></p>
                <p><a href="{{URL::action('Site\HomeController@getAboutUs')}}">درباره ما</a></p>
                @foreach($page_category as $row)
					<p><a href="{{URL::action('Site\PagesController@getList',[$row->id])}}">{{$row->title}}</a></p>
				@endforeach	
                @foreach($pages as $row)
					<p><a href="{{URL::action('Site\PagesController@getDetails',[$row->id])}}">{{$row->title}}</a></p>
				@endforeach	
            </div>
        </div>
     <br>
    </div>
    <div class="container-fluid footer_end">
        <div class="container">
            <h5 class="pull-right" style="padding-top: 15px">کلیه حقوق مطالب این سایت برای با من بگو محفوظ می باشد.</h5>
            {{--<a href="{{URL::action('Site\HomeController@getIndex')}}"><img class="pull-left" src="{{ asset('assets/uploads/setting/main/'.$setting->logo_footer)}}" alt="{{$setting->title}}" width="12%" style="padding: 10px"></a>--}}
        </div>
    </div>

</div>