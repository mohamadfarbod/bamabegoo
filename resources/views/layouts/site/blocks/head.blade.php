<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title' , $setting->title)</title>
	<meta name="keyword" content="@yield('keyword' , $setting->keyword)">
	<meta name="description" content="@yield('description' , $setting->description)">
	<meta content="yes" name="{{$setting->title}}" />
	<link rel="shortcut icon" href="{{ asset('assets/uploads/setting/main/'.$setting->favicon)}}">
	<!-- Mobile specific metas
	============================================ -->
	<meta property="og:title" content="@yield('title' , $setting->title)" />
	<meta property="og:description" content="@yield('description' , $setting->description)" />
	<meta property="og:image" content="{{ asset('assets/uploads/setting/main/'.$setting->favicon)}}" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
    <!-- Bootstrap -->
	<link href="{{asset('assets/site/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/site/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/site/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/site/css/owl.carousel.min.css')}} " rel="stylesheet">

	 <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/toastr.css')}}">
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	@yield('css')
	
</head>
