<div class="col-md-1 logo">
        <a href="{{URL::action('Site\HomeController@getIndex')}}">
			<h1 style="margin: 0">
            <img src="{{ asset('assets/uploads/setting/main/'.$setting->logo_header)}}" alt="{{$setting->title}}" width="100%">
			</h1>
		</a>
    </div>
    <div class="menu-container">
        <div class="menu container">
            <ul>
			@foreach($category as $row)
					<li><a href="">{{$row->title}}</a>
						<ul >
							@foreach($brand as $item)
								@php
									$products = App\Models\Product::whereCategoryId($row->id)->whereBrandId($item->id)->whereStatus(1)->latest()->take(6)->get();
								@endphp
								@if(count($products))
									<li><a class="sub" href="{{URL::action('Site\ProductController@getList',[$row->id,$item->id])}}">{{$item->title}}</a>
										<ul>
											@foreach($products as $product)
												<li><a href="{{URL::action('Site\ProductController@getDetails',[$product->id])}}">{{$product->title}}</a></li>
											@endforeach
										</ul>
									</li>
								@endif
							@endforeach
						</ul>
					</li>
				@endforeach
            </ul>
        </div>
    </div>
    <div class="col-md-offset-1 col-md-3 search pull-left">
        <div id="custom-search-input" class="hidden-xs hidden-sm">
			<form method="GET" action="{{URL::action('Site\ProductController@getSearch')}}">
				<div class="input-group">
					<input type="text" class="form-control" name="search" placeholder="جستجو..."/>
					<span class="input-group-btn">
						<button class="btn btn-info " type="button">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</form>
        </div>
    </div>