<div class="col-md-4 col-sm-6 col-xs-12 box-list">
	<div class="section-box-ten">
		<figure>
			<a href="{{URL::action('Site\ProductController@getDetails',[$product->id])}}"><h6>{{$product->title}}</h6></a>
			<p style="font-size: 12px">{{ $product->lead }}</p>
		</figure>
		<img src="@if(file_exists('assets/uploads/product/big/'.$product->image))
		{!! asset('assets/uploads/product/big/'.$product->image) !!}
		@else
		{!! asset('assets/uploads/notFound.jpg') !!}
		@endif " alt="{{$product->alt}}" class="img-responsive" width="100%"/>
	</div>
</div>