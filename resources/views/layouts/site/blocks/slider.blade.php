<div class="row slider">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators hidden-xs">
			@foreach($slider as $key=>$row)
			<li data-target="#myCarousel" data-slide-to="{{$key}}" @if($key==0)class="active" @endif></li>
			@endforeach
		</ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			@foreach($slider as $key=>$row)
				@php
					if(file_exists('assets/uploads/slider/big/'.$row->image)){
                        $src = asset('assets/uploads/slider/big/'.$row->image);
                    }else{
                        $src = asset('assets/uploads/notFound.jpg');
                    }
				@endphp
			<div class="item @if($key==0) active @endif">
				<img src="{{$src}}" alt="{{$row->title}}">
				{{--<div class="carousel-caption">--}}
					{{--<h4>{{$row->title}}</h4>--}}
				{{--</div>--}}
			</div>
			@endforeach
		</div>
		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			<i class="fa fa-angle-left fa-2x"></i>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" data-slide="next">
			<i class="fa fa-angle-right fa-2x"></i>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
		
		
	