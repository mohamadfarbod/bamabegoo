<script src="{{asset('assets/site/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/site/js/megamenu.js')}}"></script>
<script src="{{asset('assets/site/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/site/js/script.js')}}"></script>
 <!-- Toastr -->
<script src="{{ asset('assets/admin/js/toastr.js')}}"></script>
<!-- Validation -->
<script src="{{ asset('assets/admin/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
	@include('layouts.admin.blocks.message')
@yield('js')
