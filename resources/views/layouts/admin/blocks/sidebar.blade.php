<aside class="main-sidebar" class="print">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-right image">
                <img src="{!!asset('assets/admin/img/profile.png')!!}" class="img-circle"
                     alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name.' '.Auth::user()->family}}</p>
                <i style="color: green" class="fa fa-circle" aria-hidden="true"></i>

                آنلاین
            </div>
        </div>
		
        <ul class="sidebar-menu">

            <li class="header">ناوبری اصلی</li>
            <li>
                <a href="{{URL::action('Admin\HomeController@getIndex')}}"><i
                            class="fa fa-dashboard"></i> صفحه نخست پنل</a>
            </li>
			
            <li class="header">محتوا سایت</li>
            <li class="treeview {{\Classes\Helper::menuActive(['category','product','brand'],'admin')}}">
                <a href="#">
                    <i class="fa fa-tags"></i>
                    <span>محصولات</span>
                    <i class="fa fa-angle-left pull-left"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->hasPermission('category'))
                        <li>
                            <a href="{{URL::action('Admin\CategoryController@getIndex')}}"><i class="fa fa-circle-o"></i>
                                دسته بندی محصولات</a>
                        </li>
                    @endif
                    @if(Auth::user()->hasPermission('brand'))
                        <li>
                            <a href="{{URL::action('Admin\BrandController@getIndex')}}"><i class="fa fa-circle-o"></i>
                               دسته بندی دوم</a>
                        </li>
                    @endif
                    @if(Auth::user()->hasPermission('product'))
                        <li>
                            <a href="{{URL::action('Admin\ProductController@getIndex')}}"><i class="fa fa-circle-o"></i>
                     محصولات</a>
                        </li>
                    @endif
                    @if(Auth::user()->hasPermission('product-comment'))
                        <li>
                            <a href="{{URL::action('Admin\ProductCommentController@getIndex')}}"><i class="fa fa-circle-o"></i>
                     نظرات</a>
                        </li>
                    @endif
                </ul>
            </li>
           
            <li class="treeview {{\Classes\Helper::menuActive(['page-category','pages'],'admin')}}">
                <a href="#">
                    <i class="fa fa-file"></i>
                    <span>صفحات ایستا</span>
                    <i class="fa fa-angle-left pull-left"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->hasPermission('page-category'))
                        <li>
                            <a href="{{URL::action('Admin\PageCategoryController@getIndex')}}"><i class="fa fa-circle-o"></i>
                                دسته بندی صفحات ایستا</a>
                        </li>
                    @endif
                    @if(Auth::user()->hasPermission('pages'))
                        <li>
                            <a href="{{URL::action('Admin\PagesController@getIndex')}}"><i class="fa fa-circle-o"></i>
                     صفحات ایستا</a>
                        </li>
                    @endif
                </ul>
            </li>
           
            @if(Auth::user()->hasPermission('news'))
                <li>
                    <a href="{{URL::action('Admin\NewsController@getIndex')}}"><i
                                class="fa fa-newspaper-o"></i>
                        اخبار</a>
                </li>
            @endif

            @if(Auth::user()->hasPermission('social'))
                <li>
                    <a href="{{URL::action('Admin\SocialController@getIndex')}}"><i
                                class="fa fa-globe"></i>
                        جوامع مجازی</a>
                </li>
            @endif

            @if(Auth::user()->hasPermission('uploader'))
                <li>
                    <a target="_blank" href="{{URL::action('Admin\UploaderController@getIndex')}}"><i
                                class="fa fa-upload"></i>
                        آپلودر</a>
                </li>
            @endif
            @if(Auth::user()->hasPermission('slider'))
                <li>
                    <a  href="{{URL::action('Admin\SliderController@getIndex')}}"><i
                                class="fa fa-sliders"></i>
                        اسلایدر</a>
                </li>
            @endif

            @if(Auth::user()->hasPermission('contact-us'))
                <li>
                    <a href="{{URL::action('Admin\ContactUsController@getIndex')}}"><i
                                class="fa fa-phone"></i>
                        پیام های تماس با ما
						<small class="label pull-left bg-red">{{$count['contact_us']}}</small>
					</a>
                </li>
            @endif
            <li class="header">سیستم</li>
            <li class="treeview {{\Classes\Helper::menuActive(['user'],'admin')}}">
                <a href="#">
                    <i class="fa fa-group"></i>
                    <span>مدیران</span>
                    <i class="fa fa-angle-left pull-left"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->hasPermission('user'))
                        <li>
                            <a href="{{URL::action('Admin\UserController@getIndex')}}"><i class="fa fa-circle-o"></i>
                                مدیریت کاربران</a>
                        </li>
                        <li>
                            <a href="{{URL::action('Admin\UserController@getGroup')}}"><i class="fa fa-circle-o"></i>
                                مدیریت سطح دسترسی</a>
                        </li>
                    @endif
                </ul>
            </li>
			@if(Auth::user()->hasPermission('site-map'))
				<li>
					<a href="{{URL::action('Admin\SiteMapController@getIndex')}}"><i
								class="fa fa-cogs"></i>
						ساخت سایت مپ</a>
				</li>
			@endif
			@if(Auth::user()->hasPermission('setting'))
				<li>
					<a href="{{URL::action('Admin\SettingController@getEdit')}}"><i
								class="fa fa-cogs"></i>
						مدیریت تنظیمات سیستم</a>
				</li>
			@endif

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>